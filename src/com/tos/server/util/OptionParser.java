package com.tos.server.util;

import com.tos.server.console.manager.ConsoleManager;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * This class is a parser than given a string[] can translate it to flags(--) and options.
 * It also supports costume String<->Object.
 */
public class OptionParser {
    Map<String, Object> flags;
    Map<String, Object> options;

    /**
     * This class is an helper to build a parser.
     */
    public static class OptionParserBuilder {
        private String[] parameters;
        private Map<String, Pair<Class<?>, ?>> details;
        private Map<Class<?>, Function<String, ?>> converters;
        private boolean ignoreInvalid;

        /**
         * Constructs a builder from raw arguments.
         * @param parameters the arguments to parse.
         * @param ignoreInvalid whether an unexpected argument should error or not
         * @return the built OptionParserBuilder.
         */
        public static OptionParserBuilder create(String[] parameters, boolean ignoreInvalid) {
            OptionParserBuilder ops = new OptionParserBuilder(parameters, ignoreInvalid);
            ops.converters = new HashMap<>();
            //Add default converters.
            ops.converters.put(int.class, Integer::parseInt);
            ops.converters.put(char.class, s -> s.charAt(0));
            ops.converters.put(double.class, Double::parseDouble);
            ops.converters.put(long.class, Long::parseLong);
            return ops;
        }

        /**
         * This function registers an object that can convert from String to T.
         * @param t the class to convert from to string.
         * @param converter the object to do the conversion.
         * @param <T> the Type of t.
         */
        public <T> void registerConverter(Class<T> t, Function<String, T> converter) {
            this.converters.put(t, converter);
        }

        /**
         * Add an expectation for a parameter, and a default value should it not be present in the argument list.
         * @param name the name of the parameter expected.
         * @param clazz the class of the parameter.
         * @param defaultValue gives the argument a default value if it is not present in the argument list.
         * @param <T> the type of the parameter.
         * @return this.
         */
        public <T> OptionParserBuilder add(String name, Class<T> clazz, T defaultValue) {
            if (details == null) details = new HashMap<>();
            details.put(Objects.requireNonNull(name),
                    new Pair<>(Objects.requireNonNull(clazz), defaultValue));
            return this;
        }

        /**
         * Add an expectation for a flag, and a default value should it not be present in the argument list.
         * @param name the name of the flag expected.
         * @param defaultValue gives the argument a default value if it is not present in the argument list.
         * @return this.
         */
        public OptionParserBuilder addFlag(String name, boolean defaultValue) {
            if (details == null) details = new HashMap<>();
            details.put(Objects.requireNonNull(name), new Pair<>(null, defaultValue));
            return this;
        }

        /**
         * This function starts the parsing prccess.
         * @return the created parser.
         */
        public OptionParser build() {
            return new OptionParser(parameters, details, converters);
        }

        /**
         * Constructs an OptionParserBuilder without default conversions.
         * @param parameters the arguments to parse.
         * @param ignoreInvalid whether an unexpected argument should error or not
         */
        private OptionParserBuilder(String[] parameters, boolean ignoreInvalid) {
            this.parameters = parameters;
            this.details = null;
            this.ignoreInvalid = ignoreInvalid;
        }
    }

    /**
     * Constucts a parsed OptionParser with arguments.
     * @param options the argument given to the program.
     * @param details the details about the expected parameters and flags.
     * @param converter a map of converter from string various types.
     */
    protected OptionParser(String[] options, Map<String, Pair<Class<?>, ?>> details, Map<Class<?>, Function<String, ?>> converter) {
        flags = new HashMap<>();
        this.options = new HashMap<>();
        if (options == null) return;
        //Initiating all expected argument to the defaultValue.
        for (Map.Entry<String, Pair<Class<?>, ?>> entry : details.entrySet()) {
            if (entry.getValue().getKey() == null) {
                if((Boolean) entry.getValue().getValue())
                    flags.put(entry.getKey(), null);
            } else
                this.options.put(entry.getKey(), entry.getValue().getValue());
        }

        //Parsing
        for (String s : options) {
            String trimmed = s.trim();
            //If it doesn't start with -- it's not valid.
            if (trimmed.startsWith("--")) {
                //Split arguments from flags
                //Arguments have an A=B pattern
                String[] ts = trimmed.split("=");
                String name = ts[0].substring(2).trim();
                Pair<Class<?>, ?> c = details.get(name);
                //If there is something on the other side of the = than it's an argument else it's a flag.
                if (c.getKey() == null) {
                    if ((Boolean) c.getValue())
                        flags.put(name, null);
                } else {
                    Function<String, ?> converterFunc = converter.get(c.getKey());
                    if (converterFunc != null)
                        this.options.put(name, converterFunc.apply(ts[1]));
                    else
                        this.options.put(name, ts[1]);
                }
            } else {
                ConsoleManager.instance.println(trimmed + " is not a valid argument");
            }
        }
    }


    /**
     * @param flag the name of the flag
     * @return true if the flag is not set and false otherwise.
     */
    public boolean getNFlag(String flag) {
        return !flags.containsKey(flag);
    }

    /**
     * @param flag the name of the option
     * @return true if the flag is not set and false otherwise.
     */
    public boolean getFlag(String flag) {
        return flags.containsKey(flag);
    }

    /**
     * @param name the name of the option
     * @return the value of the option.
     */
    public long getLong(String name) {
        return (long) options.get(name);
    }

    /**
     * @param name the name of the option
     * @return the value of the option.
     */
    public int getInt(String name) {
        return (int) options.get(name);
    }

    /**
     * @param name the name of the option
     * @return the value of the option.
     */
    public String getString(String name) {
        return (String) options.get(name);
    }

    /**
     * @param name the name of the option
     * @return the value of the option.
     */
    public double getDouble(String name) {
        return (double) options.get(name);
    }

    /**
     * @param name the name of the option
     * @return the value of the option.
     */
    public <T> T getOption(String name) {
        return (T) options.get(name);
    }
}
