package com.tos.server.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a utility class for string actions.
 */
public final class StringUtil {

    /**
     * This function splits a string by regex to a list, if it has a regex at the end it leaves an empty string.
     *
     * @param s     the string to split.
     * @param regex the regex to split the string at.
     * @return a list of the separated strings.
     */
    public static List<String> split(String s, String regex) {
        //A variable to store the results to.
        List<String> splitList = new ArrayList<>();
        //Compile a pattern, mostly for performance.
        Matcher matcher = Pattern.compile(regex).matcher(s);
        //The start(index) of the next string to cut..
        int next = 0;
        //Find the first occurrence of the regex.
        matcher.find();
        //The end(index) of the next string to split.
        int end = matcher.start();
        //While there are still places matching the regex.
        while (!matcher.hitEnd()) {
            //Add the string contained within the regexes.
            splitList.add(s.substring(next, end));
            //The start of the next region to cut.
            next = matcher.end();
            //If there is another regex find the area contained between the current one and it.
            //Otherwise cut from the current regex to the end of the string.
            if (matcher.find())
                end = matcher.start();
            else
                splitList.add(s.substring(next));
        }
        return splitList;
    }

    /**
     * This function splits a string once based on pattern to an array.
     *
     * @param source the string to split.
     * @param pattern the pattern to split the string with.
     * @return the strings cut from the source string.
     */
    public static String[] splitOnce(String source, Pattern pattern) {
        Matcher matcher = pattern.matcher(source);
        if (source.isEmpty() || !matcher.find()) return new String[]{source};

        int next = matcher.end();
        return new String[]{source.substring(0, next), source.substring(next)};
    }

    /**
     * This function splits a string once by a pattern appending the results to the list given.
     * @param source the string to split.
     * @param pattern the pattern to split the string with.
     * @param append the list to append the results to.
     */
    public static void splitOnce(String source, Pattern pattern, List<String> append) {
        Matcher matcher = pattern.matcher(source);
        if (source.isEmpty() || !matcher.find()) {
            append.add(source);
            return;
        }

        int next = matcher.end();
        append.add(source.substring(0, next));
        append.add(source.substring(next));
    }
}