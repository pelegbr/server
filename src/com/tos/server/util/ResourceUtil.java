package com.tos.server.util;

import com.google.gson.*;
import com.tos.server.api.IJsonReplaceable;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.Server;
import com.tos.server.Statics;
import sun.security.util.Cache;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Map;

/**
 * This class is a part of a templating mechanism it replaces values within a json object.
 */
public class ResourceUtil {
    //Cache of a loaded file.
    public static final Cache<String, JsonElement> fileCache = Cache.newSoftMemoryCache(36, 1800);
    //A parser for reading the json file.
    private static JsonParser parser = new JsonParser();

    /**
     * This function loads reads and replaces value in a json file.
     * @param filePath the path to the json file
     * @param replaceables the replacers objects that need to replace certain fields.
     * @return the evaluated Json file
     */
    @SafeVarargs
    public static JsonElement readJson(String filePath, IJsonReplaceable<JsonElement>... replaceables) {
        //Try to pull the file from the cache.
        JsonElement jsonFile = Statics.cacheEnabled ? fileCache.get(filePath) : null;
        if (jsonFile == null) {
            //If fail re-load the file and parse it.
            try {
                InputStream stream = Server.class.getResourceAsStream("resources" + filePath);
                if (stream == null) return null;

                jsonFile = parser.parse(new InputStreamReader(stream));
                if (jsonFile == null) return null;

                if (Statics.cacheEnabled)
                    fileCache.put(filePath, jsonFile);
            } catch (Exception e) {
                ConsoleManager.instance.println(e);
                Arrays.asList(e.getStackTrace()).forEach(te -> ConsoleManager.instance.println("&@r" + te));
            }
        }

        //Deep copy the JsonObject before modifying it and then replace all the fields that need replacement in it.
        JsonObject object = (JsonObject) deepCopy(jsonFile);
        if (object != null) {
            for (IJsonReplaceable<JsonElement> replaceable : replaceables) {
                for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                    object.add(entry.getKey(), replaceable.replace(entry.getKey(), entry.getValue()));
                }
            }
        }
        return object;
    }

    /**
     * This function copies a whole JsonObject(Recursively).
     * @param element the JsonElement to copy.
     * @return
     */
    public static <T extends JsonElement> T deepCopy(T element) {
        //If it's an array deep copy all it's elements.
        if (element instanceof JsonArray) {
            JsonArray array = new JsonArray();
            for (JsonElement el : (JsonArray) element)
                array.add(deepCopy(el));
            return (T) array;
        }
        //If it's an Object deep copy all it's fields.
        if (element instanceof JsonObject) {
            JsonObject result = new JsonObject();
            for (Map.Entry<String, JsonElement> entry : ((JsonObject) element).entrySet())
                result.add(entry.getKey(), deepCopy(entry.getValue()));
            return (T) result;
        }
        //if it's none of the above then it's a JsonPrimitive and it's safe not to copy it since it's immutable.
        return element;
    }
}