package com.tos.server.util;

import com.tos.server.api.annotation.ApiMember;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ApiMember(Versions = "@null")
/**
 * This class is a utility for http methods.
 */
public final class HttpUtil {
    /**
     * This function parses an http GET request.
     * @param quary the query requested by the client. Should look like: http://www.website.com/a=1&b=2
     * @return a map of the parameters given by the client.
     */
    public static Map<String, String> getParameters(String quary) {
        //If the query is empty return an empty map.
        if (quary == null || quary.isEmpty()) return Collections.emptyMap();
        Map<String, String> properties = new HashMap<>();
        quary = quary.trim();
        //Ignore all indexers.
        quary = quary.charAt(0) == '/' ? quary.substring(1) : quary;

        //Parse the parameters there are separated by & and given value with =.
        for (String s : quary.split("&")) {
            String[] Property = s.split("=");
            if (!Property[0].isEmpty())
                properties.put(Property[0], Property.length < 2 ? "" : Property[1]);
        }

        return properties;
    }

    /**
     * This is a convenience function that wraps {@link #getParameters(String)}
     * @param quary the query
     * @return the parameters given by the client.
     */
    public static Map<String, String> getParameters(URI quary) {
        return getParameters(quary.getQuery());
    }

}
