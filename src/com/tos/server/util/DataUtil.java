package com.tos.server.util;

import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.Statics;
import javafx.util.Pair;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiFunction;

/**
 * The DataUtil class automates the IO to and from the internal database structure of the program.
 */
public final class DataUtil {
    /**
     * This function imports data from other data structures to the internal data structure.
     * @param fileDir the location of the file to import.
     * @param table the table to update in the data base.
     * @param inputAdapter an adapter that changes the input based on index, name and column.
     * @return true if the action succeeded and false if it failed.
     */
    public static boolean importData(String fileDir, String table, Pair<List<Integer>, BiFunction<String, Integer, String>> inputAdapter) {
        //Get the extension of the file to know the format.
        String ext = fileDir.substring(fileDir.lastIndexOf('.') + 1);
        //Search for a class that support the file's extension.
        FormatType type = FormatType.ofExtension(ext);
        if (type == null) {
            //if no format was found warn and return failure(false).
            ConsoleManager.instance.println("Could not find a support for " + ext + " method.");
            return false;
        } else {
            try {
                //erase the table being updated.
                DBUtil.erase(table);
                //Add the information recovered from the file to the now empty table.
                DBUtil.save(table, type.load(fileDir, inputAdapter, Statics.in));
            } catch (SQLException | IOException ex) {
                //if the information was not saved or recovered from the file error and return failure(false).
                ConsoleManager.instance.println("Couldn't export DataBase to " + fileDir + ".");

                ConsoleManager.instance.println("&@rException in thread \"" + Thread.currentThread().getName() + "\"" + ex);
                StackTraceElement[] trace = ex.getStackTrace();
                Arrays.stream(trace).forEach(s -> ConsoleManager.instance.println("&@r" + s));

                return false;
            }
            //success.
            return true;
        }
    }

    /**
     * This function exports data from internal data structure to another data structures .
     * @param fileDir the location of the file to export to.
     * @param table the table to get data out of.
     * @param outputAdapter an adapter that changes the output based on index, name and column.
     * @return true if the action succeeded and false if it failed.
     */
    public static boolean exportData(String fileDir, String table, Pair<List<Integer>, BiFunction<String, Integer, String>> outputAdapter) {
        //Get the extension of the file to know the format.
        String ext = fileDir.substring(fileDir.lastIndexOf('.') + 1);
        //Search for a class that support the file's extension.
        FormatType type = FormatType.ofExtension(ext);
        if (type == null) {
            //if no format was found warn and return failure(false).
            ConsoleManager.instance.println("Could not find a support for " + ext + " method.");
            return false;
        } else {
            try {
                //convert the data and save it.
                type.save(fileDir, DBUtil.load(table, outputAdapter), Statics.out);
            } catch (SQLException | IOException ex) {
                //if the information was not saved or extracted from the database error and return failure(false).
                ConsoleManager.instance.println("&@rCouldn't export DataBase to " + fileDir + ".");

                ConsoleManager.instance.println("&@rException in thread \"" + Thread.currentThread().getName() + "\"" + ex);
                StackTraceElement[] trace = ex.getStackTrace();
                Arrays.stream(trace).forEach(s -> ConsoleManager.instance.println("&@r" + s));

                return false;
            }
            //success.
            return true;
        }
    }

    /**
     * This class represent a Format that is supported to be converted from and to the program's internal structure.
     */
    public static abstract class FormatType {
        /**
         * This function converts a file with the format of this type to a Table(list of lists of strings).
         * @param fileDir the location of the file to convert to from this format to a Table if the stream is null.
         * @param outputAdapter an adapter that changes the output based on index, name and column.
         * @param stream the input to convert, if null defaults to new FileStream(fileDir).
         * @return A table(list of lists of strings) of the converted data.
         * @throws IOException if the file or stream fail.
         */
        public abstract List<List<String>> load(String fileDir, Pair<List<Integer>, BiFunction<String, Integer, String>> outputAdapter, InputStream stream) throws IOException;

        /**
         * This function converts a Table(list of lists of strings) to a file with the format of this type.
         * @param fileDir the location of the file to write to if the stream is null.
         * @param data the table to convert to this format.
         * @param stream the object to output the file converted, if null defaults to new FileStream(fileDir).
         * @throws IOException if the writing to the stream/file has failed.
         */
        public abstract void save(String fileDir, List<List<String>> data, OutputStream stream) throws IOException;

        // The first supported format, csv.
        public static FormatType cvs = new FormatType("csv") {

            /**
             * This function converts a csv file to a Table(list of lists of strings).
             * @param fileDir the location of the file to convert to from this format to a Table if the stream is null.
             * @param outputAdapter an adapter that changes the output based on index, name and column.
             * @param stream the input to convert, if null defaults to new FileStream(fileDir).
             * @return A table(list of lists of strings) of the converted data.
             * @throws IOException if the file or stream fail.
             */
            @Override
            public List<List<String>> load(String fileDir, Pair<List<Integer>, BiFunction<String, Integer, String>> outputAdapter, InputStream stream) throws IOException {
                //If the stream is null use the file location give.
                if (stream == null) {
                    if (!fileDir.substring(fileDir.lastIndexOf(".") + 1).equals("csv")) return null;
                    stream = new FileInputStream(fileDir);
                }
                //Initialize the output
                List<List<String>> ret = new ArrayList<>();
                //Read the whole file.
                byte[] b = new byte[4096];
                while (stream.read(b, b.length - 4096, 4096) != -1) {
                    byte[] temp = b;
                    b = new byte[temp.length + 4096];
                    System.arraycopy(temp, 0, b, 0, temp.length);
                }
                //Convert to Lines.
                String[] s = new String(SerialUtil.trimBytes(b)).split("\n");
                //Store the headers(column names).
                ret.add(Arrays.asList(s[0].split(",")));
                for (int i = 1; i < s.length; i++) {

                    List<String> line = new ArrayList<>();
                    //Split the lines by , and add each string result to the table.
                    String[] split = s[i].split(",");

                    //Process data in the adapter.
                    for (int j = 0; j < split.length; j++) {
                        String tr = split[j].trim();
                        if (outputAdapter == null)
                            line.add(tr);
                        else
                            line.add(outputAdapter.getKey().contains(j) ? outputAdapter.getValue().apply(tr, j) : tr);
                    }

                    ret.add(line);
                }
                stream.close();
                return ret;
            }

            /**
             * This function converts a Table(list of lists of strings) to a csv file.
             * @param fileDir the location of the file to write to if the stream is null.
             * @param data the table to convert to this format.
             * @param stream the object to output the file converted, if null defaults to new FileStream(fileDir).
             * @throws IOException if the writing to the stream/file has failed.
             */
            @Override
            public void save(String fileDir, List<List<String>> data, OutputStream stream) throws IOException {
                //If the stream is null use the file location give.
                if (stream == null) {
                    File f = new File(fileDir);

                    if (f.getParentFile() == null || !f.getParentFile().exists() || !f.exists()) {
                        f.mkdirs();
                    }
                    f.createNewFile();
                    stream = new FileOutputStream(f);
                }

                Iterator<List<String>> it = data.iterator();
                Iterator<String> cur = it.next().iterator();
                //Write the headers.
                stream.write(cur.next().getBytes());
                while (cur.hasNext()) {
                    stream.write(("," + cur.next()).getBytes());
                }
                stream.write("\n".getBytes());
                //Write the rest of the data.
                while (it.hasNext()) {
                    cur = it.next().iterator();
                    while (cur.hasNext())
                        stream.write((cur.next() + ",").getBytes());
                    stream.write('\n');
                }
                //Close the stream.
                stream.flush();
                stream.close();
            }
        };

        //The supported formats.
        protected static Map<String, FormatType> extensionsMap;

        //The constructor takes what extensions the format type supports.
        protected FormatType(String... supportedExtensions) {
            if (extensionsMap == null) extensionsMap = new HashMap<>();
            for (String s : supportedExtensions)
                FormatType.extensionsMap.put(s, this);
        }

        //Finds what object handles the requested extension.
        public static FormatType ofExtension(String ext) {
            return extensionsMap.get(ext);
        }

    }
}
