package com.tos.server.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * This class automates some aspects of byte-stream-string conversion.
 */
public final class SerialUtil {
    /**
     * This function converts a byte array to a minimal string(without any the trailing 0s)
     *
     * @param bytes the array to make in to a string.
     * @return the string stripped off of any trailing 0s.
     */
    public static byte[] trimBytes(byte[] bytes) {
        //Start at the end until you observe a 0.
        for (int i = bytes.length; i > 0; i--) {
            if (bytes[i - 1] != 0)
                return Arrays.copyOf(bytes, i);
        }
        return bytes;
    }

    /**
     * This function reads a stream at chucks of 4kilobytes.
     *
     * @param stream the stream to read.
     * @param close  whether this function should close the stream after using it.
     * @return the read bytes.
     * @throws IOException
     */
    public static byte[] readStream(InputStream stream, boolean close) throws IOException {
        //Create a new chuck of 4k.
        byte[] result = new byte[4096];
        //Read chuck
        int read = stream.read(result);
        //Is the chuck used whole by the read operation(so there aren't anymore bytes to read)
        if (read == 4096) {
            //A new chuck to read to.
            byte[] temp = new byte[4096];
            //While the chuck was used whole by the read operation(so there may some bytes to read).
            while (read == 4096) {
                //Read chuck
                read = stream.read(temp);
                //-1 is an end of file Token.
                if (read == -1) break;
                byte[] ref = result;
                //Expend the byte array.
                result = new byte[result.length + read];
                System.arraycopy(ref, 0, result, 0, ref.length);
                System.arraycopy(temp, ref.length, result, 0, read);
            }
        } else {
            //Copy only the bytes modified by the stream.
            byte[] ref = result;
            result = new byte[read];
            System.arraycopy(ref, 0, result, 0, read);
        }
        //Close the stream if requested by the callee.
        if (close) stream.close();
        return result;
    }
}
