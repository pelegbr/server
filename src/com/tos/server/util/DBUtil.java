package com.tos.server.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.Statics;
import javafx.util.Pair;

import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.function.BiFunction;

/**
 * This class control IO with the database.
 * It also offers some utilitarian functions like {@link #addTests(String, JsonArray)} and {@link #removeTests(String, JsonArray)}.
 */
public final class DBUtil {
    //Maintain a map for when a clean-up/app-close is called and removing duplication.
    private static Map<String, Connection> connectionMap = new HashMap<>();
    //Caching statement made frequently stored for performance.
    private static PreparedStatement[] statements = new PreparedStatement[3];
    //Garbage-collection for Results that are not close.
    private static List<ResultSet> unclosedSets = new ArrayList<>();
    //The dynamic Prepared statement pool, kept for mostly Garbage collection.
    private static List<PreparedStatement> toGC;

    //private constructor so no one can initiate this class, mostly for practice sake.
    private DBUtil() {
    }

    /**
     * This function validates login details.
     *
     * @param args the Argument used for login.
     * @return The message to give the requesting client, "Pass" for password error, "Usr" for incorrect user, or null for no error.
     */
    public static String checkUserDetails(Map<String, String> args) {
        //Caching the login query statement.
        if (statements[0] == null) {
            try {
                statements[0] = Statics.dbConnection.prepareStatement("SELECT Name,Password FROM Users WHERE Name = ? COLLATE NOCASE");
            } catch (SQLException sqlEx) {
                ConsoleManager.instance.println("Couldn't Create a Prepared Statement, Exception: " + sqlEx.getMessage());
            }
        }
        //The message returned to the client.
        String ret = null;
        try {
            //Set the variable in the statement( to replace the ? in the SQL statement you see above).
            statements[0].setString(1, args.get("Username"));
            //Execute the query and store results in the set variable.
            ResultSet set = statements[0].executeQuery();
            //If the set is null or empty than there's no such field.
            if (set != null && !set.isClosed()) {
                //Error: Pass, Incorrect password.
                ret = Objects.equals(set.getString("Password"), args.get("Password")) ? null : "Pass";
                //Close the set after using it to free allocated resourced.
                set.close();
            } else {
                //Error: Usr, incorrect/non-existent user.
                ret = "Usr";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //return the message.
        return ret;
    }

    /**
     * This function connects with a database and caches connection for garbage-collection and not duplicating a connection.
     *
     * @param path the path to the database file.
     * @return the Connection made/query from cache to the database.
     */
    public static Connection connectDB(String path) {
        //If in cache don't reconnect.
        Connection connection = connectionMap.get(path);
        if (connection != null) return connection;
        try {
            //Open a new connection to this file.
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
            //Safety check to not cut a local file.
            if (path.lastIndexOf("/") != -1)
                ConsoleManager.instance.println("Successfully connected to database " + path.substring(path.lastIndexOf("/") + 1));
            //Put in cache.
            connectionMap.put(path, connection);
        } catch (SQLException sqlEx) {
            //If the connetion failed error.
            ConsoleManager.instance.println("Can't Connect to database, Exception: " + sqlEx.getMessage());
        }
        return connection;
    }

    /**
     * This function closes the DataBase if it's not closed already.
     *
     * @param path the path to the database file.
     */
    public static void closeDB(String path) {
        //Check if it was connected to.
        Connection c = connectionMap.get(path);
        if (c == null) return;
        try {
            //Garbage collecting.
            for (PreparedStatement statement : statements)
                if (statement != null && !statement.isClosed() && statement.getConnection() == c)
                    statement.close();
            if (toGC != null)
                for (PreparedStatement statement : toGC)
                    if (!statement.isClosed() && statement.getConnection() == c)
                        statement.close();

            if (unclosedSets != null)
                for (ResultSet set : unclosedSets) {
                    if (!set.getStatement().isClosed() && set.getStatement().getConnection() == c) {
                        set.getStatement().close();
                        set.close();
                    }
                }
            //Remove from Cache and close if .
            connectionMap.remove(path).close();
        } catch (SQLException sqlEx) {
            System.err.println("Can't Disconnect from database, Exception: " + sqlEx.getMessage());
        }
    }

    /**
     * This function executes dynamic updates to the database.
     *
     * @param connection the connection to the database.
     * @param query      the SQL query to execute.
     */
    public static void execute(Connection connection, String query) {
        //If the connection is null return, protects from a Null Poiner Exception.
        if (connection == null) return;
        try {
            //Creating a new statement.
            Statement statement = connection.createStatement();
            //Execute the requested query.
            statement.execute(query);
            //Close statement and release resources.
            statement.close();
        } catch (SQLException sqlEx) {
            ConsoleManager.instance.println("&@r" + sqlEx.getMessage());
        }
    }

    /**
     * This function dynamic query to the database.
     *
     * @param connection the connection to the database.
     * @param query      the SQL query you wish to perform on the database.
     * @return
     */
    public static ResultSet get(Connection connection, String query) {
        ResultSet set = null;
        if (connection != null) {
            try {
                //Create a statement, query and store the results in set.
                set = connection.createStatement().executeQuery(query);
                //Add the set to the uncloseSets List for later if it's not closed.
                unclosedSets.add(set);
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
        return set;
    }

    /**
     * This function Closes a set and remove it from the garbage collection list.
     *
     * @param set the set to close.
     */
    public static void closeSet(ResultSet set) {
        //remove set from the garbage collection list
        unclosedSets.remove(set);
        try {
            //close the set's statement.
            set.getStatement().close();
            //close the actual set.
            set.close();
        } catch (SQLException ex) {
            ConsoleManager.instance.print(ex);
        }
    }

    /**
     * This function inserts given data to a row.
     *
     * @param connection the connection to the database.
     * @param table      the name of the table.
     * @param include    the variables included in the insertion query.
     * @param parameters the data to insert to the database.
     */
    public static void insert(Connection connection, String table, String include, Object... parameters) {
        //Prevent a null pointer exception.
        if (connection == null) return;
        //Constructs the update statement in parts.
        StringBuilder builder = new StringBuilder(String.format("INSERT INTO %s(%s) VALUES(", table, include));
        for (int i = 0; i < parameters.length; i++) {
            Object o = parameters[i];
            //If the object is a string add '' to it.
            if (o instanceof String) builder.append('\'').append(o).append('\'');
                //Else just put the object as is.
            else
                builder.append(o);
            //If the there are more parameters put a ',' in front of the parameter.
            if (i != parameters.length - 1)
                builder.append(",");
        }
        //Close the statement and execute it.
        builder.append(");");
        execute(connection, builder.toString());
    }

    /**
     * This function rollbacks the database given to it.
     *
     * @param connection the connection to the database to rollback.
     */
    public static void rollback(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException sqlEx) {
            ConsoleManager.instance.println("Failed to rollback reason: " + sqlEx.getMessage());
        }
    }

    /**
     * This is a utility to add a test, the program's purpose.
     *
     * @param username the username to add to which the test.
     * @param array    the json serialization of the tests to add.
     * @return null if no error occurred, json error it if did.
     */
    public static String addTests(String username, JsonArray array) {
        JsonArray first = (JsonArray) array.get(0);
        PreparedStatement statement = null;
        PreparedStatement select = null;

        try {
            //Prepare the statements to update the database.
            select = Statics.dbConnection.prepareStatement("SELECT StudentId FROM Tests WHERE StudentId = ?");
            statement = Statics.dbConnection.prepareStatement("INSERT INTO Tests(UserId,StudentId,ProId) VALUES( ?,?,?)");
            //Query the id of the student.
            ResultSet set = DBUtil.get(Statics.dbConnection, "SELECT Id FROM Users WHERE Name = \"" + username + "\"");
            //Set the variable(?) for the id column.
            statement.setInt(1, set.getInt("Id"));
            //Close the set and free allocated resources since it's not used anymore.
            DBUtil.closeSet(set);
        } catch (SQLException sql) {
            //Warn if has any error.
            ConsoleManager.instance.println(sql);
        }
        String s = null;
        JsonArray second = (JsonArray) array.get(1);
        try {
            //Iterate over all tests in the json.
            for (JsonElement element : second) {
                //
                select.setInt(1, element.getAsInt());
                ResultSet set = select.executeQuery();
                //User is already signed to 2 or more tests... report an error.
                if (set.next() && set.next()) {
                    //Query the student's real name.
                    ResultSet studentName = get(Statics.dbConnection, "SELECT Name FROM Students WHERE Id = " + element.getAsInt());
                    String name = studentName.getString("Name");
                    closeSet(studentName);
                    s = "{\"ERR\":\"" + name + "\"}";
                }
                //Release set since it's been used.
                set.close();
                //Insert the test with the details uncovered.
                statement.setInt(2, element.getAsInt());
                statement.setInt(3, first.get(0).getAsInt());
                statement.execute();
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return the status.
        return s;
    }

    /**
     * This function is prepares a statement, and adds it to the garbage collection list.
     *
     * @param connection the connection to the database.
     * @param sql        the query for the statement.
     * @return a statement prepared if succeed, null otherwise.
     */
    public static PreparedStatement prepare(Connection connection, String sql) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            if (toGC == null) toGC = new ArrayList<>();
            toGC.add(statement);
        } catch (SQLException ex) {
            ConsoleManager.instance.println(ex);
        }
        return statement;
    }

    /**
     * This function is a utility function for removing tests.
     *
     * @param username the username to remove tests for.
     * @param checked  the tests to remove.
     */
    public static void removeTests(String username, JsonArray checked) {
        try {
            //query the username's Id and store it in set.
            ResultSet set = get(Statics.dbConnection, "SELECT Id FROM Users WHERE Name = \"" + username + "\"");
            //Get the Id
            if (set == null) return;
            int id = set.getInt("Id");
            //Prepare the statement for deleting tests if not ready.
            if (statements[1] == null)
                statements[1] = Statics.dbConnection.prepareStatement("DELETE FROM Tests WHERE StudentId = ? AND UserId = " + id);

            //Delete each test scheduled in json.
            for (JsonElement element : checked) {
                statements[1].setInt(1, element.getAsInt());
                statements[1].execute();
            }
        } catch (SQLException ex) {
            ConsoleManager.instance.println(ex);
        }
    }

    /**
     * This function puts data from a file to a table.
     * @param table the table to insert the data to
     * @param data the data to put in the table.
     * @throws SQLException if the insertion failed.
     */
    public static void save(String table, List<List<String>> data) throws SQLException {
        //If the data is empty return.
        if (data == null || data.isEmpty()) return;

        //Prepare a query to insert the data.
        StringBuilder quarry = new StringBuilder("INSERT OR REPLACE INTO " + table + "(" + data.get(0).get(0));

        //Append the data to the right places in the query.
        for (int i = 1; i < data.get(0).size(); i++) quarry.append(",").append(data.get(0).get(i));
        quarry.append(") VALUES(?");

        //Append wildcards as much as there are columns.
        for (int i = 0; i < data.get(0).size() - 1; i++) quarry.append(",?");
        quarry.append(")");

        //Prepare the actual statement.
        PreparedStatement statement = Statics.dbConnection.prepareStatement(quarry.toString());
        Iterator<List<String>> l = data.iterator();
        l.next();

        //Iterate over all the data and insert it in the right index in the statement.
        while (l.hasNext()) {
            List<String> o = l.next();
            int i = 1;
            for (String par : o)
                statement.setObject(i++, par);
            statement.execute();
        }
        //release the resources allocated to the statement.
        statement.close();
    }

    /**
     * This function loads data from the database to a table(list of lists of string).
     * @param tableName the name of the table to load
     * @param outputAdapter an adapter that changes the output based on index, name and column.
     * @return A table(list of lists of string) containing all the data in the table.
     * @throws SQLException if it failed to load the data from the database.
     */
    public static List<List<String>> load(String tableName, Pair<List<Integer>, BiFunction<String, Integer, String>> outputAdapter) throws SQLException {
        List<List<String>> data = new ArrayList<>();
        //Query every field in the table.
        ResultSet s = get(Statics.dbConnection, "SELECT * FROM " + tableName);
        //Query the columns names in the table.
        ResultSet columns = get(Statics.dbConnection, "PRAGMA table_info(" + tableName + ")");

        //Add the first 'row' in the table- the 'headers'
        data.add(new ArrayList<>());
        int columnCount = 0;
        //Queries the column name of each column and inserts it to the headers of the table.
        while (columns.next()) {
            columnCount++;
            data.get(0).add(outputAdapter != null ? outputAdapter.getValue().apply(columns.getString("name"), 0) : columns.getString("name"));
        }
        //Release the resources of columns.
        closeSet(columns);

        //Query the data in multiple row
        while (s.next()) {
            //Give the data to the adapter so it can modify it before saving it in the line.
            List<String> line = new ArrayList<>(columnCount);
            for (int i = 0; i < columnCount; i++) {
                if (outputAdapter != null && outputAdapter.getKey().contains(i))
                    line.add(outputAdapter.getValue().apply(s.getObject(i + 1).toString(), i + 1));
                else
                    line.add(s.getObject(i + 1).toString());
            }
            data.add(line);
        }
        //Release the resources in s.
        closeSet(s);

        return data;
    }

    /**
     * This function erases a table in the database.
     * @param table the name of the table.
     * @throws SQLException
     */
    public static void erase(String table) throws SQLException {
        //Reset the meta-info and table info in the database.
        Statement statement = Statics.dbConnection.createStatement();
        statement.execute("UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='" + table + "'");
        statement.execute("DELETE FROM " + table);
        //Release the statement once closed.
        statement.close();
    }

    /**
     * This function queries the expiration date of a security token given to a browser.
     * @param token the token given to the browser.
     * @return the Date at which the token given expires
     */
    public static Date getTokenExpirationDate(String token) {
        if (token != null)
            try {
                //Prepare a new statement if not in cache.
                if (statements[2] == null)
                    statements[2] = Statics.dbConnection.prepareStatement("SELECT ExDate FROM Users WHERE Token = ?");
                //Setting the variables(?) in the SQL.
                statements[2].setString(1, token);
                //Query the statement and store in set.
                ResultSet set = statements[2].executeQuery();
                set.next();
                Date exDate = set.getDate(1);
                //release the resources allocated to set.
                set.close();
                return exDate;
            } catch (SQLException ex) {

            }
        return null;
    }

    /**
     * This function write a token generate to a user in the database to keep track of it.
     * @param token the token given to the user.
     * @param date the data the token expires at.
     * @param username the username of the user.
     * @param password the password of the user.
     */
    public static void writeToken(String token, Date date, String username, String password) {
        //TODO...log
        if (token == null) return;
        try {
            //Prepare a statement if not in cache already.
            if (statements[3] == null)
                statements[3] = Statics.dbConnection.prepareStatement("UPDATE Users Token = ?,LastDate = ? Where Username = ? AND Password = ?");
            //Set the variables(?).
            statements[3].setString(1, token);
            statements[3].setDate(1, date);
            statements[3].setString(1, username);
            statements[3].setString(1, password);
            //Write to database.
            statements[3].execute();
        } catch (SQLException ex) {
        }
    }
}