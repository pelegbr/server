package com.tos.server;

public final class Enc {
    private final int key;

    public Enc(byte[] key) {
        this.key = key.length;
    }

    public byte[] enc(byte[] data) {
        return data;
    }

    public byte[] dec(byte[] data) {
        return data;
    }
}
