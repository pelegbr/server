package com.tos.server;

import com.tos.server.api.annotation.OnFail;
import com.tos.server.provider.ResponseProvider;

/**
 * A handler for when a provider finished a request.
 */
public final class EventHandler {
    //TODO: impl
    public static void Success(ResponseProvider provider) {

    }

    @OnFail
    public static void Fail(ResponseProvider provider) {

    }
}
