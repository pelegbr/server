var changed = true;
var checked = [];
//get the username if you logged in already.
var username = getCookie("Username")||sessionStorage.getItem("Username");

//initialize all fields in the login form, if necessary.
window.onload = function(event){
    if(!skip){
        login.onclick = handleClick;
        checkStorage();

        Username.onkeypress = function(e) {
            handleKeys(e, Username);
        };

        Password.onkeypress = function(e) {
            handleKeys(e, Password);
        };
    }
}

//add a test to the list of tests to remove.
function checkBox(el){
    if(checked.indexOf(el.value) > -1){
        checked.splice(checked.indexOf(el.value));
        if(checked.length == 0)
            document.querySelector("#removeTests").disabled = true;
    }else{
        checked.push(el.value);
        document.querySelector("#removeTests").disabled = false;
    }
}

//check if the browser contains the information relevent to the login and if so, switch to the main page.
function checkStorage(){
    var username = getCookie("Username");
    var pass = getCookie("Password");
    if(username && pass){
        var http = new XMLHttpRequest();
        var url = 'get_data.php';
        var params = 'requestType=login' + '&Username=' + username + '&Password=' + pass;
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if(http.readyState == 4 && http.status == 200){
                if(http.responseText == "Pass" || http.responseText == "Usr")
                    hider.hidden = false;
                    //TODO: delete cookies
                else{
                    document.querySelector("#loginDiv").innerHTML = "";
                    var a = eval('(' + http.responseText + ')');
                    receiveAppend(a, document.querySelector("#loginDiv"));
                    window.history.pushState("object or string", "Title", "/");
                    document.title = "main";
                }
            }
        }
        http.send(params);
    }else{
        username = sessionStorage.getItem("Username");
        pass = sessionStorage.getItem("Password");
        if(username && pass){
            var http = new XMLHttpRequest();
            var url = 'get_data.php';
            var params = 'requestType=login' + '&Username=' + username + '&Password=' + pass;
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            http.onreadystatechange = function() {
            if(http.readyState == 4 && http.status == 200){
                if(http.responseText == "Pass" || http.responseText == "Usr")
                    hider.hidden = false;
                    //TODO: delete storage.
                else{
                    document.querySelector("#loginDiv").innerHTML = "";
                    var a = eval('(' + http.responseText + ')');
                    receiveAppend(a, document.querySelector("#loginDiv"));
                    window.history.pushState("object or string", "Title", "/");
                    document.title = "main";
                    }
                }
            }
            http.send(params);
        }else
            hider.hidden = false;
    }
}

//save data in cookies.
function setCookie(name,cookie){
    var date = new Date();
    date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + date.toUTCString();
    document.cookie = name + "=" + cookie + ";" + expires + ";path=/;";
}

//get a cookie from storage.
function getCookie(cookieName){
    var ca = document.cookie.split(';');
    for(var v in ca){
        while(ca[v].charAt(0) == ' ')
            ca[v] = ca[v].substring(1);
        if(ca[v].indexOf(cookieName) == 0)
            return ca[v].substring(cookieName.length+1,ca[v].length)
    }
}

//check if a field really changed before you can click the login button.
var handleKeys = function(e, a) {
    changed = a.value != a.lastValue;
    a.lastValue = a.value;
    if (e.keyCode == 13)
        handleClick();
    return true;
};
//checks if a string is empty
function isEmpty(str) {
    return (!str || 0 === str.length);
}
//send a request to login and if the succeeded load the main page.
function handleClick() {
    if (changed == true && checkDetails()) {
        var http = new XMLHttpRequest();
        var url = 'get_data.php';
        var params = 'requestType=login' + '&Username=' + Username.value + '&Password=' + Password.value;
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                if (document.querySelector(".error-msg")) document.querySelector(".error-msg").remove();
                switch (http.responseText) {
                    case "Usr":
                        if (document.querySelector(".error-msg") == null) {
                            var el = document.createElement("span");
                            var tx = document.createTextNode("Unrecognized username.");
                            el.appendChild(tx);
                            el.setAttribute("role", "alert");
                            el.setAttribute("class", "error-msg");
                            document.querySelector(".userDiv").appendChild(el);
                            window.history.pushState("object or string", "Title", "/");
                        }
                        break;
                    case "Pass":
                        if (document.querySelector(".error-msg") == null) {
                            var el = document.createElement("span");
                            var tx = document.createTextNode("Wrong password. Try again.");
                            el.appendChild(tx);
                            el.setAttribute("role", "alert");
                            el.setAttribute("class", "error-msg");
                            document.querySelector(".passDiv").appendChild(el);
                            window.history.pushState("object or string", "Title", "/");
                        }
                        break;
                    default:
                        if(rememberMe.checked){
                            setCookie("Username",Username.value);
                            setCookie("Password",Password.value)
                        }else{
                            sessionStorage.setItem("Username",Username.value);
                            sessionStorage.setItem("Password",Password.value);
                        }
                        username = Username.value;
                        document.querySelector("#loginDiv").innerHTML = "";
                        var a = eval('(' + http.responseText + ')');
                        receiveAppend(a, document.querySelector("#loginDiv"));
                        window.history.pushState("object or string", "Title", "/");
                        document.title = "main";
                        break;
                }
            }
        };
        http.send(params);
        window.history.pushState("object or string", "Title", "/loggingIn");
    }
}
//check the login details are present if not, alert the user.
function checkDetails() {
    if (isEmpty(Username.value)) {
        if (document.querySelector(".error-msg"))
            document.querySelector(".error-msg").remove();
            if (document.querySelector(".error-msg") == null) {
                var el = document.createElement("span");
                var tx = document.createTextNode("Please insert your username");
                el.appendChild(tx);
                el.setAttribute("role", "alert");
                el.setAttribute("class", "error-msg");
                document.querySelector(".userDiv").appendChild(el);
            }
        return false;
    }

    if (isEmpty(Password.value)) {
        if (document.querySelector(".error-msg"))
            document.querySelector(".error-msg").remove();
            if (document.querySelector(".error-msg") == null) {
                var el = document.createElement("span");
                var tx = document.createTextNode("Please insert your password");
                el.appendChild(tx);
                el.setAttribute("role", "alert");
                el.setAttribute("class", "error-msg");
                document.querySelector(".passDiv").appendChild(el);
            }
        return false;
    }
    return true;
}

//send a request to remove the chosen tests.
function removeChosenTests(){
    var send = "me=" + username + "&checked=[" + checked + "]";
    sendRequest('/','removeTests',send,function(a){});
    checkStorage();
}

//navigate to the test request page.
function go2MB(){
    var http = new XMLHttpRequest();
    var params = 'requestType=new2nd';
    http.open('POST', 'get_data.php', true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    document.title = "יצירת מועד ב' חדש.";
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200){
            document.querySelector("#loginDiv").innerHTML = "";
            var a = eval('(' + http.responseText + ')');
            receiveAppend(a, document.querySelector("#loginDiv"));
            window.history.pushState("object or string", "Title", "/2ndTest");
        }
    }
    http.send(params);
}