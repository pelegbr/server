var manager = {"send":[],"ignore":false};
skip = true;
//initialize all elements with a class drop-trigger.
init(document.querySelectorAll(".drop-trigger"));

//handle a click on the body.
//disables dropdown if it is active.
document.body.onclick = function(e){
    if(manager.ignore)
        manager.ignore = false;
	else if(e.target != manager.active && manager.active != null){
	    manager.active.dropdown.style.display = "none";
	    manager.active = null;
	}
}

//create a dropdown menu for all element given.
function init(obj){
    for(var v in obj){
        if(typeof obj[v] == "object"){
            createDrop(obj[v]);
        }
    }
}

//creates a dropdown menu object for the given element.
function createDrop(el){
    el.onclick = el.onfocus = function(){
        if(manager.active != this)
            activate(this);
    };

    el.onkeyup = function(e){
        filter(e,el);
    }

    el.dropdown = document.createElement("div");
    el.dropdown.className = "dropdown-content";
    el.dropdown.style.display = "none";
    appendAfter(el,el.dropdown);

    if(!manager.tracked){
        manager.tracked = [];
        receiveChoices(el);
        activate(el);
    }else
        el.disabled = true;

    el.index = manager.tracked.length;
    el.style.hidden = manager.tracked.length > 0;
    manager.tracked.push(el);
    return el;
}

//process the data given by the user and send it to the server.
function receiveChoices(input){
    var send = [];
    for(var i = 0;i < input.index;i++){
        send.push(manager.tracked[i].selected.value);
    }
    input.dropdown.innerHTML = "";
    sendRequest(null,'sendChoices',"chose=[" + send + "]",function(a){
        for(var i = 0;i < a.length;i++){
            var element = document.createElement('a');
            element.innerHTML = a[i].text;
            element.value = a[i].value;
            element.onclick = function(){
                select(input,this);
                manager.ignore = true;
            };
            input.dropdown.appendChild(element);
        }
    });
}

//activate a dropdown menu.
function activate(input) {
    if(manager.active)
        manager.active.dropdown.style.display = "none";
	manager.active = input;
	input.disabled = false;
	input.dropdown.style.display = "block";
    updateDisplay(input);
    input.focus();
}

//determine what effect a keypress should have.
function filter(e,input){
    var displayables = getDisplayable(input.dropdown);
    switch(e.keyCode){
        case 13:
            if(displayables.length == 1)
                select(input,displayables[0]);
            else if(input.selected != null)
                select(input,input.selected);
        break;
        case 38:
            if(input.selected == null) input.selected = displayables[displayables.length-1];
            var index = displayables.indexOf(input.selected);
            if(index > 0)
                input.selected = displayables[index-1];
            else if(index == 0)input.selected = displayables[0];
        break;
        case 40:
            if(input.selected == null) input.selected = displayables[0];
            var index = displayables.indexOf(input.selected);
            if(index < displayables.length -1)
                input.selected = displayables[index+1];
            else if(index == 0)input.selected = displayables[0];
        break;
        default:
            if(input.selected != null && displayables.length < input.selected.value)
                input.selected = null;
            updateDisplay(input);
            var table = document.querySelector("table");
            if(table)table.style.display = "none";
    }
}

//returns displayable indexes in the array
function getDisplayable(element){
    var displayables = [];
    for(var i = 0;i < element.childNodes.length;i++){
        if(element.childNodes[i].style.display != "none")
            displayables.push(element.childNodes[i]);
    }
    return displayables;
}

//filter the option that are allowed to be shown after a keypress.
function updateDisplay(input){
    var options = input.dropdown.childNodes;
    for(var i = 0;i< options.length;i++){
        if (options[i].innerHTML.toUpperCase().indexOf(input.value.toUpperCase()) > -1) {
            options[i].style.display = "";
        } else {
        	options[i].style.display = "none";
        }
    }
}

//select one of the active option in a dropdown menu and closes it.
function select(input,value){
	input.selected = value;
    input.value = value.innerHTML;
    input.dropdown.style.display = "none";
    if(manager.tracked.length - 1 > input.index){
        receiveChoices(manager.tracked[input.index + 1]);
        activate(manager.tracked[input.index + 1]);
    }else if(input.index == manager.tracked.length-1)
        receiveTable();
}

//creates a table to contain all the students that match the data given.
function receiveTable(){
    var send = [];
    for(var i = 0;i < manager.tracked.length;i++){
        send.push(manager.tracked[i].selected.value);
    }
    manager.send[0] = send;
    manager.send[1] = [];
    var table = document.querySelector("studTable");
    if(table == null){
        table = document.createElement("table");
        var row = document.createElement("tr");
        var headerBoxes = document.createElement("th");
        var headerName = document.createElement("th");
        headerName.innerHTML = "שם";
        headerName.style.fontSize = "large";
        row.appendChild(headerBoxes);
        row.appendChild(headerName);
        table.appendChild(row);
    }else{
        for(var i = 1;i < table.childNodes.length; i++){
            table.removeChild(table.childNodes[i]);
        }
    }
    var classes = ["a","b"];
    sendRequest('/2ndTest','sendTable',"chose=[" + send + "]",function(a){
        var curRow;
        for(var i = 0;i < a.length;i++){
            if(i % 2 == 0){
                if(curRow != null)
                    table.appendChild(curRow);
                curRow = document.createElement("tr")
                curRow.className = classes[(i / 2)% 2];
            }
            var obj = document.createElement("td");
            if(i % 2 == 0){
                var inner = document.createElement("input");
                inner.setAttribute("type","checkbox");
                inner.setAttribute("name","sChoice");
                inner.setAttribute("value",a[i]);
                inner.onclick = function(){ clickBox(this);};
                obj.appendChild(inner);
            }else{
                obj.innerHTML = a[i];
                obj.style.fontSize = "large";
            }
            curRow.appendChild(obj);
        }
        if(curRow != null)
            table.appendChild(curRow);
    });
    loginDiv.insertBefore(table,sendTests);
    manager.active.blur();
    manager.active = null;
}

//add a student to the list to add tests for.
function clickBox(el){
    if(manager.send[1] && manager.send[1].indexOf(el.value) > -1){
        manager.send[1].splice(manager.send[1].indexOf(el.value));
        if(manager.send[1].length == 0)
            document.querySelector("#sendTests").disabled = true;
    }else{
        manager.send[1].push(el.value);
        document.querySelector("#sendTests").disabled = false;
    }
}

//after the form is filled send a request to add the tests to the server.
function sendNewTests(){
    var params = "me=" + username + "&par=[[" + manager.send[0] + "],[" +manager.send[1] + "]]";
    sendRequest('/','newTests',params ,function(a){
        if(a.ERR){
            var err = a.ERR.indexOf("ERR:");
            openDialog("שגיעת רישום.",".התלמיד: " + a.ERR + " לא יכל להירשם מכוון שכבר נרשם ל-2 מועדים או יותר","נסה שוב.","בטל הרשמה. ",function(e){
                if(!e){
                    checkStorage();
                }
            });
        }
        else{
            document.querySelector("#loginDiv").innerHTML = "";
            receiveAppend(a, document.querySelector("#loginDiv"));
        }
    });
}