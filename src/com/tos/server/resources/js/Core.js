//Client-side templating
var skip;
function template(name,json,unSerialized){
    var elements =  [];
    var dataName = "@" + name;
    for(var i = 0;i < unSerialized[dataName].length;){
        var el = json.cloneNode(true);
        while(replaceNext(el,unSerialized[dataName][i],"%%"))i++;
        elements.push(el);
    }
    return elements;
}

function replaceNext(object,replacement,what){
    if(object.className == "%%"){
        object.className = replacement;
        return true;
    }
    if(object.innerHTML == "%%"){
        object.innerHTML = replacement;
        return true;
    }
    for(var v in object.attributes){
        if(object.attributes[v].nodeValue == what){
            object.attributes[v].nodeValue = replacement;
            return true;
        }
    }

    var tr = false;
    for(var v in object.childNodes){
        if(tr |= replaceNext(object.childNodes[v],replacement,what))
            return tr;
    }
    return tr;
}

function receiveAppend(json,parent){
    for(var obj in json){
        parent.appendChild(receive(json[obj]));
    }
}

function receive(json){
    var el = document.createElement(json.htmlType);
    for (var obj in json){
		if (obj == "htmlType") continue;
		else if(obj == "htmlText") el.innerHTML += json[obj];
		else if(json[obj].htmlType) el.appendChild(receive(json[obj]));
        else if(obj.startsWith("$style"))
            el.style[obj.substring(6,obj.length)] = json[obj];
        else if(obj.startsWith('#')){
            var d = template(obj.substring(1),receive(json[obj][obj.substring(1)]),json[obj]);
            for(var g in d){
                el.appendChild(d[g]);
            }
        } else if(json.htmlType != null){
            if(obj == "class") el.className = json[obj];
            else if(json.htmlType != null) el.setAttribute(obj,json[obj]);
        }
    }
    return el;
}

//utility function to open a dialog box.
function openDialog(title,message,accept,deny,callback){
    if(document.getElementById("myModal").style.display != "none") document.getElementById("myModal").style.display = "none";
    document.getElementById("myModal").style.display = "block";
    document.getElementById("title").innerHTML = title;
    document.getElementById("message").innerHTML = message;
    document.getElementById("accept").innerHTML = accept;
    document.getElementById("deny").innerHTML = deny;
    document.getElementById("accept").onclick = function(){document.getElementById("myModal").style.display = "none";callback(true);}
    document.getElementById("deny").onclick = function(){document.getElementById("myModal").style.display = "none";callback(false);}
}
//utility function to append a DOM-Element after another
function appendAfter(element,appended){
    if (element.nextSibling)
        element.parentNode.insertBefore(appended,element.nextSibling);
    else
        element.parentNode.appendChild(appended);
}

//utility function to send a request to the server(POST).
function sendRequest(url,request,parameters,func){
    var http = new XMLHttpRequest();
    var params = 'requestType='+request + "&" + parameters;
    http.open('POST', 'get_data.php', true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if(http.readyState == 4 && http.status == 200){
            var a = eval('(' + http.responseText + ')');
            func(a);
            if(url)
                window.history.pushState("object or string", "Title", url);
        }
    }
    http.send(params);
}