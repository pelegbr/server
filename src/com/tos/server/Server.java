package com.tos.server;

import com.sun.net.httpserver.HttpServer;
import com.tos.server.api.Http.VirtualHandler;
import com.tos.server.api.annotation.ImplMember;
import com.tos.server.console.commands.Common.CommonCommands;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.provider.CommandProvider;
import com.tos.server.provider.JsonProvider;
import com.tos.server.provider.PageProvider;
import com.tos.server.util.DBUtil;
import com.tos.server.util.OptionParser;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;


@ImplMember(Versions = "@all")
//The main Class.
public class Server {
    //The delegate Handler, it filters the input and sends it to the correct provider.
    public final static VirtualHandler handler = new VirtualHandler(Statics.fileServer, new PageProvider(), new JsonProvider(), new CommandProvider());
    //The OI of the program.
    public static HttpServer server;

    //Entry point.
    public static void main(String[] args) {
        //parsing arguments for settings like caching and console window.
        OptionParser parser = OptionParser.OptionParserBuilder.create(args, false).addFlag("noCache", false)
                .addFlag("noConsole", false).add("port", int.class, 8080).build();

        //Querying the parser for the arguments.
        Statics.cacheEnabled = parser.getNFlag("noCache");
        Statics.consoleEnabled = parser.getNFlag("noConsole");
        Statics.port = parser.getInt("port");
        Statics.port = Statics.port < 160 ? 8080 : Statics.port;

        //clean-up and close the connection to the database.
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                DBUtil.closeDB(Statics.runningPath + "/jb.db")
        ));

        //Support for more languages via wider encoding.
        System.setProperty("file.encoding", "UTF-8");

        //Setup IO and http handling.
        setup();
    }

    /**
     * This function sets up the IO of the program.
     */
    private static void setup() {
        //start console if it's needed.
        if (Statics.consoleEnabled) {
            ConsoleManager.instance.start();
            //subscribe the console manager to add the basic commands.
            CommonCommands.register(ConsoleManager.instance);
        }

        //Connect to the database and store the connection(Using SQLITE3).
        Statics.dbConnection = DBUtil.connectDB(Statics.runningPath + "/jb.db");
        //Open the database in WAL mode.
        DBUtil.execute(Statics.dbConnection, "PRAGMA journal_mode=WAL;");

        long l = System.nanoTime();
        try {
            //create a basic IO server for http requests.
            server = HttpServer.create(new InetSocketAddress(Statics.port), 0);
            //add a listener for when the server gets a request, delegating it to the providers.
            server.createContext("/", handler.Create());
            //Fix the executor so that the server creates a new dedicated executor.
            server.setExecutor(null);
            //start the IO server.
            server.start();
            //print the time it took to start up if the console is enabled.
            if (Statics.consoleEnabled)
                ConsoleManager.instance.println("Setup in " + (Statics.startup = (System.nanoTime() - l) / 1000000.0) + " ms at localhost:" + server.getAddress().getPort());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}