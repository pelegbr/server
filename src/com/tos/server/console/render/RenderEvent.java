package com.tos.server.console.render;

import com.tos.server.console.Console;

import java.awt.*;

/**
 * This class is a render event to handle, it contains all the information needed to render to the console.
 */
public class RenderEvent {
    //The graphics object of the console window.
    public final Graphics graphics;
    //The console to render to.
    public final Console console;

    /**
     * @param graphics the graphics object of the console window.
     * @param console the console to render to.
     */
    public RenderEvent(Graphics graphics, Console console) {
        this.graphics = graphics;
        this.console = console;
    }
}
