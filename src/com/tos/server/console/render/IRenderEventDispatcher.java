package com.tos.server.console.render;

/**
 * This class is an interface for a class that renders a console window.
 */
public interface IRenderEventDispatcher {
    /**
     * This function handles a render event.
     * @param event the event to handle.
     * @return true if the event was handled and should not continue to the next handler, false otherwise.
     */
    boolean OnRender(RenderEvent event);
}
