package com.tos.server.console.render;

import com.tos.server.console.Console;

import java.awt.Color;
import java.util.List;

/**
 * This class is the default implementation of {@link IRenderEventDispatcher} and renders the text on the console.
 */
public class RenderEvenDispatcher implements IRenderEventDispatcher {
    /**
     * This function handles rendering the text of the console.
     * @param event the event to handle.
     * @return true always.
     */
    @Override
    public boolean OnRender(RenderEvent event) {
        //Get the lines in the console's buffer.
        List<String> lines = event.console.getLines();
        //Set the text colour to white.
        event.graphics.setColor(Color.WHITE);

        for (int i = 0; i < Console.LineWrap && i < event.console.getLines().size(); i++) {
            //Get the current string to display by adding i and the console's scroll.
            String current = lines.get(i + event.console.getScroll() / 20);
            if (current.startsWith("&@") && current.length() >= 3) {
                //Coloured text handling.
                event.graphics.setColor(event.console.getManager().getMapped(current.charAt(2)));
                current = current.length() > 3 ? current.substring(3) : "";
            }
            //Draw the text at the right line in the console.
            event.graphics.drawString(current, 20, i * 20 - event.console.getScroll() % 20 + 40);
        }

        //Reset the colour to write and draw the input.
        event.graphics.setColor(Color.WHITE);
        event.graphics.drawString(event.console.getTemp(), 20, 580);

        return true;
    }
}
