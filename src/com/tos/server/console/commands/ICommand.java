package com.tos.server.console.commands;

import com.tos.server.console.Console;

/**
 * This interface represent a functionality of a console command.
 * @param <T> the type returned by this command.
 */
public interface ICommand<T> {
    /**
     * This function calls this command.
     * @param console the console IO for this command.
     * @param parameters the parameters for this command.
     * @return the result of the command.
     */
    T exec(Console console, Object... parameters);

    /**
     * @return The executable associated with this command.
     */
    ICommandExec<T> getExec();
}