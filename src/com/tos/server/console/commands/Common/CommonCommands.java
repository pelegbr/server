package com.tos.server.console.commands.Common;

import com.tos.server.console.Console;
import com.tos.server.console.commands.ICommand;
import com.tos.server.console.commands.ICommandExec;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.provider.FileServer;
import com.tos.server.Statics;
import com.tos.server.util.DBUtil;
import com.tos.server.util.DataUtil;
import com.tos.server.util.ResourceUtil;
import javafx.util.Pair;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Stream;

/**
 * This class contains most of the common commands
 */
public final class CommonCommands {
    private final static Map<String, List<ICommand<?>>> commands = new HashMap<>();

    /**
     * This function creates all the common commands.
     */
    private static void setup() {
        if (!commands.isEmpty()) return;
        //Add a command to flush the caches.
        addCommands("revalidate", new CommonCommand<String>() {
            @Override
            public String exec(Console console, Object... parameters) {
                ResourceUtil.fileCache.clear();
                FileServer.fileCache.clear();
                return "revalidate json Cache.";
            }
        });

        //Add a command to color the console
        addCommands("colorme", new CommonCommand<String>(Optional.class) {
                    @Override
                    public String exec(Console console, Object... parameters) {
                        Color color;
                        if (parameters[0] instanceof Integer) {
                            color = new Color((int) parameters[0]);
                        } else {
                            String s = (String) parameters[0];
                            if (s.equalsIgnoreCase("black"))
                                color = Color.black;
                            else {
                                color = ConsoleManager.instance.getMapped(s.toLowerCase().charAt(0));
                                if (color.equals(Color.white)) color = Color.black;
                            }

                        }
                        ConsoleManager.instance.getMain().setBackgroundColour(color);
                        return "Color has been changed...";
                    }
                }
                , new CommonCommand<Void>() {

                    @Override
                    public Void exec(Console console, Object... parameters) {
                        console.inputColour();
                        return null;
                    }
                });

        //Add a command to close the server.
        addCommands("close", new CommonCommand<Void>(Integer.class) {
            @Override
            public Void exec(Console console, Object... parameters) {
                System.exit(((Number) parameters[0]).intValue());
                return null;
            }
        }, new CommonCommand<Void>() {
            @Override
            public Void exec(Console console, Object... parameters) {
                System.exit(0);
                return null;
            }
        });

        //Add a command to to print the startup time.
        addCommands("startup", new CommonCommand<String>() {
            @Override
            public String exec(Console console, Object... parameters) {
                return "Server started in: " + Statics.startup + "ms";
            }
        });

        //Add a command to rollback the server database.
        addCommands("rollback", new CommonCommand<String>() {
            @Override
            public String exec(Console console, Object... parameters) {
                DBUtil.rollback(Statics.dbConnection);
                return "Your DataBase has been rolled back.";
            }
        });

        //Add a command to import data to database.
        addCommands("import", new CommonCommand<String>(String.class, String.class) {
            @Override
            public String exec(Console console, Object... parameters) {
                long s = System.nanoTime();
                Pair<List<Integer>, BiFunction<String, Integer, String>> inputAdapter;
                //Sets the input adapter based on the request.
                switch (((String) parameters[1]).trim()) {
                    case "Students":
                        inputAdapter = new Pair<>(Collections.singletonList(0), (e, index) -> {
                            if (Character.isDigit(e.charAt(2)))
                                return ((e.charAt(0) == 'ט' ? 100 : 200) + Integer.parseInt(e.substring(2))) + "";
                            return ((e.charAt(1) == 'א' ? 300 : 400) + Integer.parseInt(e.substring(3))) + "";
                        });
                        break;
                    default:
                        inputAdapter = null;
                }
                //Call importData
                return DataUtil.importData((String) parameters[0], (String) parameters[1], inputAdapter) ?
                        "Successfully import DataBase from " + parameters[1] + " in " + (System.nanoTime() - s) / 1000000.0 + "ms" : "Failed to import from DataBase to " + parameters[1];
            }
        });

        //Add a command to export data from database.
        addCommands("export", new CommonCommand<String>(String.class, String.class) {
            @Override
            public String exec(Console console, Object... parameters) {
                long s = System.nanoTime();
                Pair<List<Integer>, BiFunction<String, Integer, String>> outputAdapter = null;
                ResultSet usernameQuery = DBUtil.get(Statics.dbConnection, "SELECT Name FROM Users INNER JOIN Tests WHERE Id = UserId");
                ResultSet nameQuery = DBUtil.get(Statics.dbConnection, "SELECT Name FROM Students INNER JOIN Tests WHERE Id = StudentId");
                ResultSet professionQuery = DBUtil.get(Statics.dbConnection, "SELECT Name FROM Professions INNER JOIN Tests WHERE Id = ProId");
                //Sets the output adapter base on the request.
                switch ((String) parameters[1]) {
                    case "Tests":
                        outputAdapter = new Pair<>(Arrays.asList(0, 1, 2), (e, t) -> {
                            switch (t) {
                                case 0:
                                    //Translate the Database text.
                                    switch (e) {
                                        case "UserId":
                                            return "שם משתמש";
                                        case "ProId":
                                            return "מקצוע";
                                        case "StudentId":
                                            return "תלמיד";
                                    }
                                case 1:
                                    //Get the username
                                    try {
                                        assert usernameQuery != null;
                                        usernameQuery.next();
                                        return usernameQuery.getString("Name");
                                    } catch (SQLException ex) {
                                        console.println(ex);
                                        return null;
                                    }
                                case 2:
                                    //Get the name of the student having a test.
                                    try {
                                        assert nameQuery != null;
                                        nameQuery.next();
                                        return nameQuery.getString("Name");
                                    } catch (SQLException ex) {
                                        console.println(ex);
                                        return null;
                                    }
                                case 3:
                                    //Get the profession name.
                                    try {
                                        assert professionQuery != null;
                                        professionQuery.next();
                                        return professionQuery.getString("Name");
                                    } catch (SQLException ex) {
                                        console.println(ex);
                                        return null;
                                    }
                            }
                            return null;
                        });
                }
                //Caling exportData and printing text appropriate to the action's success.
                String ret = DataUtil.exportData((String) parameters[0], (String) parameters[1], outputAdapter) ?
                        "Successfully exported DataBase to " + parameters[1] + " in " + (System.nanoTime() - s) / 1000000.0 + "ms" : "Failed to export DataBase to ";
                //cleanup.
                DBUtil.closeSet(nameQuery);
                DBUtil.closeSet(usernameQuery);
                DBUtil.closeSet(professionQuery);
                return ret;
            }
        });
    }

    /**
     * This function adds commands to the commands Map
     * @param name the name of the commands.
     * @param commands the functionality of the commands.
     */
    private static void addCommands(String name, ICommand<?>... commands) {
        CommonCommands.commands.put(name, Arrays.asList(commands));
    }

    /**
     * This function adds all the commands inside the command map to a ConsoleManager.
     * @param manager the ConsoleManager to add the commands to.
     */
    public static void register(ConsoleManager manager) {
        setup();
        for (Map.Entry<String, List<ICommand<?>>> entry : commands.entrySet())
            for (ICommand<?> command : entry.getValue())
                manager.register(entry.getKey(), command);
    }

    /**
     * @return a stream of all command in the commands map.
     */
    public static Stream<ICommandExec<?>> stream() {
        setup();
        return commands.values().stream().flatMap(List::stream).map(ICommand::getExec);
    }

    /**
     * @param name the name of the commands to stream.
     * @return a stream of all commands sharing the same name.
     */
    public static Stream<ICommandExec<?>> stream(String name) {
        setup();
        List<ICommand<?>> candidates = commands.get(name);
        return candidates == null ? null : candidates.stream().map(ICommand::getExec);
    }

    /**
     * This function find a command by name and parameters.
     * @param name the name of the command.
     * @param parameter the parameters that command takes.
     * @return the command it found if it didn't, null.
     */
    public static ICommandExec<?> find(String name, Object... parameter) {
        setup();
        List<ICommand<?>> list = commands.get(name);
        return list == null ? null : list.stream().map(ICommand::getExec).filter(cmd -> cmd.accepts(parameter)).findFirst().orElse(null);
    }
}
