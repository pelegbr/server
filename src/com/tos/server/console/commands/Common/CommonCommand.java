package com.tos.server.console.commands.Common;

import com.tos.server.console.Console;
import com.tos.server.console.commands.ICommand;
import com.tos.server.console.commands.ICommandExec;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This is an abstract class to help with creating new commands.
 * It makes filtering easier, so you only need to override one function.
 *
 * @param <T> the Type returned by the command.
 */
public abstract class CommonCommand<T> implements ICommand<T> {
    //An empty(does nothing accepts everything) command.
    public static final ICommand<?> EMPTY = new ICommand() {
        @Override
        public Object exec(Console console, Object... parameters) {
            return null;
        }

        @Override
        public ICommandExec getExec() {
            return new ICommandExec() {
                @Override
                public Object exec(Console console, Object... parameters) {
                    return null;
                }

                @Override
                public boolean accepts(Object... parameters) {
                    return true;
                }

                @Override
                public boolean accepts(Class[] parametersType) {
                    return true;
                }
            };
        }
    };
    CommandExec exec;

    /**
     * This constructor filters parameters base on classes.
     *
     * @param c the types of parameters accepted by a command.
     */
    public CommonCommand(Class... c) {
        this.exec = new CommandExec(c);
    }


    /**
     * @return The executable associated with this command.
     */
    public CommandExec getExec() {
        return exec;
    }

    /**
     * This class implements {@link ICommandExec} for basic class-based filtering.
     */
    public class CommandExec implements ICommandExec<T> {
        //The accepted parameter types.
        List<Class<?>> acceptedClasses;

        /**
         * This constuctor takes the classes accepted by the command and builds a filter list.
         *
         * @param c the types of the parameters accepted by the command.
         */
        public CommandExec(Class<?>... c) {
            acceptedClasses = Arrays.asList(c);
        }

        /**
         * This function calls for the contained command object's exec.
         *
         * @param console    the console IO for this command.
         * @param parameters the parameters for this command.
         * @return the result of the command.
         */
        @Override
        public T exec(Console console, Object... parameters) {
            return CommonCommand.this.exec(console, parameters);
        }


        /**
         * This function determines whether given parameters suffice for a command.
         * @param parameters the parameters to evaluate stuffiest.
         * @return true if found compatible, false if not
         */
        @Override
        public boolean accepts(Object... parameters) {
            if (parameters.length != acceptedClasses.size()) return false;
            AtomicInteger i = new AtomicInteger(0);
            return acceptedClasses.stream().allMatch(c -> {
                Object o = parameters[i.incrementAndGet()];
                return c == Optional.class || c.isInstance(o) || (Number.class.isAssignableFrom(c) && o instanceof Number);
            });
        }

        @Override
        public boolean accepts(Class<?>... parameterTypes) {
            if (parameterTypes.length != acceptedClasses.size()) return false;
            AtomicInteger i = new AtomicInteger(0);
            return acceptedClasses.stream().allMatch(c -> {
                Class cls = parameterTypes[i.incrementAndGet()];
                return c == Optional.class || c.isAssignableFrom(cls) || (Number.class.isAssignableFrom(cls) && Number.class.isAssignableFrom(cls));
            });
        }
    }
}
