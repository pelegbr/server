package com.tos.server.console.commands;

import com.tos.server.console.Console;

/**
 * This interface represents an executable associated with a command.
 *
 * @param <T> the type returned by the contained command.
 */
public interface ICommandExec<T> {
    /**
     * This function calls for the contained command object's exec.
     *
     * @param console    the console IO for this command.
     * @param parameters the parameters for this command.
     * @return the result of the command.
     */
    T exec(Console console, Object... parameters);

    /**
     * This function determines whether given parameters suffice for a command.
     * @param parameters the parameters to evaluate stuffiest.
     * @return true if found compatible, false if not
     */
    boolean accepts(Object... parameters);

    /**
     * This function determines whether given parameters suffice for a command.
     * @param parametersType the parameter types to evaluate stuffiest.
     * @return true if found compatible, false if not
     */
    boolean accepts(Class<?>... parametersType);
}
