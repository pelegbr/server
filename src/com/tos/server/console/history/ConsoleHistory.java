package com.tos.server.console.history;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

/**
 * This class is the default implementation for {@link IConsoleHistory} used by the console to keep record of various thing such as inputs, scroll, ect'...
 */
public class ConsoleHistory implements IConsoleHistory {
    //The scroll of the console.
    private int Scroll = 0;
    //The index of the last.
    private int index = 0;
    //The Current input field of the console.
    private AtomicReference<String> current = new AtomicReference<>("");//"";
    //The input history object.
    private List<String> history = new ArrayList<>();

    /**
     * @return the amount the console is scrolled by.
     */
    @Override
    public int getScroll() {
        return Scroll;
    }

    /**
     * This function scrolls the console by a given amount
     *
     * @param amount the amount to scroll the console.
     */
    @Override
    public void scroll(int amount) {
        Scroll += amount;
    }

    /**
     * @return the index of the current input string.
     */
    @Override
    public int getIndex() {
        return index;
    }

    /**
     * @return the current input.
     */
    @Override
    public AtomicReference<String> getCurrent() {
        return current;
    }


    /**
     * @return the input history buffer
     */
    @Override
    public List<String> getHistory() {
        return history;
    }

    /**
     * This function pushes the current input to the history buffer.
     *
     * @param clear whether the input should be cleared.
     */
    @Override
    public void push(boolean clear) {
        history.add(current.get());
        index = history.size() - 1;
        if (clear)
            current.set("");
    }

    /**
     * @return a stream of the history buffer.
     */
    @Override
    public Stream<String> steam() {
        return history.stream();
    }

    /**
     * This function retrieves the previous input and puts it in the input field.
     */
    @Override
    public void previous() {
        if (history.isEmpty() || index < 0 || index > history.size()) return;
        if (!history.get(index).equals(current.get())) {
            push(false);
        }
        if (index > 0)
            current.set(history.get(--index));
    }

    /**
     * This function retrieves the next input and puts it in the input field.
     */
    @Override
    public void next() {
        if (history.isEmpty() || history.size() <= index + 1) return;
        if (!history.get(index).equals(current.get()))
            push(false);
        if (index < history.size() && index >= 0)
            current.set(history.get(++index));
    }


    /**
     * This function deletes the input, and if full is true also the history buffer.
     * @param full whether the history buffer should be emptied.
     */
    @Override
    public void clear(boolean full) {
        current.set("");
        Scroll = 0;
        if (full) {
            history.clear();
            index = 0;
        }
    }

}
