package com.tos.server.console.history;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

/**
 * This is an interface for an object containing the console's input history, scroll, current line and inputs.
 */
public interface IConsoleHistory {
    /**
     * @return the scroll of the console.
     */
    int getScroll();

    /**
     * This function scrolls the console by a given amount
     * @param amount the amount to scroll the console.
     */
    void scroll(int amount);

    /**
     * @return the index of the current input string.
     */
    int getIndex();

    /**
     * @return the current input.
     */
    AtomicReference<String> getCurrent();

    /**
     * @return the input history buffer
     */
    List<String> getHistory();

    /**
     * This function pushes the current input to the history buffer.
     * @param clear whether the input should be cleared.
     */
    void push(boolean clear);

    /**
     * @return a stream of the history buffer.
     */
    Stream<String> steam();

    /**
     * This function retrieves the previous input and puts it in the input field.
     */
    void previous();

    /**
     * This function retrieves the next input and puts it in the input field.
     */
    void next();

    /**
     * This function deletes the input, and if full is true also the history buffer.
     * @param full whether the history buffer should be emptied.
     */
    void clear(boolean full);
}
