package com.tos.server.console.util;

import com.tos.server.console.commands.ICommandExec;
import com.tos.server.console.Console;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * This class is a utility for parsing the command syntax for the console.
 */
public final class ParserUtil {
    //A filter that allows only letters digits brackets and commas.
    public static final Pattern commandFilter = Pattern.compile("[^a-zA-Z(,)0-9.]");

    /**
     * This function parses a command syntax for a query.
     *
     * @param c          the console that the query came from.
     * @param command    the command text.
     * @param parameters the list to insert the parsed parameters at.
     * @return A command executable.
     */
    public static ICommandExec Parse(Console c, String command, List<Object> parameters) {
        String commandInput = commandFilter.matcher(command).replaceAll("");
        //Checking if it is a command.
        if (commandInput.contains("(") && commandInput.contains(")")) {
            //The name of the command in the query.
            String comName = commandInput.substring(0, commandInput.indexOf("("));
            //Separates the parameter text from the other text in the query.
            String params = commandInput.substring(commandInput.indexOf("(") + 1, commandInput.indexOf(")"));
            //If the string is not empty, parse it.
            if (!params.isEmpty())
                Collections.addAll(parameters, parseArgs(params.split(",")));
            //Find the command.
            return c.getManager().getCommand(comName, parameters.toArray());
        }

        return null;

    }

    /**
     * This function parses arguments given to it by the form of a String[] to an Object[].
     * @param split the arguments to parse.
     * @return the parsed arguments.
     */
    private static Object[] parseArgs(String[] split) {
        //Allocate a new array to contain each parameter.
        Object[] parameters = new Object[split.length];
        for (int i = 0; i < split.length; i++) {
            //Get the current argument to parsed.
            String s = split[i];

            try {
                //Is it a decimal number.
                if (s.contains(".")) {
                    //Parse as a double.
                    Double d = Double.parseDouble(s);
                    if (d - d.floatValue() < d / Math.pow(2, 30))
                        //If it can be of float precision use a float.
                        parameters[i] = d.floatValue();
                    else
                        //Else use double
                        parameters[i] = d;

                } else {
                    //Parse as a long.
                    Long l = Long.parseLong(s);
                    if (l <= Integer.MAX_VALUE)
                        //If l can be contained inside an integer use an integer.
                        parameters[i] = l.intValue();
                    else
                        //Else use a long.
                        parameters[i] = l;
                }
            } catch (NumberFormatException nE) {
                //If it can't be parsed it is a string.
                parameters[i] = s;
            }
        }
        return parameters;
    }
}
