package com.tos.server.console;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;

/**
 * This class is a listener that gets notified for all event in the console window.
 * Overrides All method from {@link KeyListener} {@link FocusListener} {@link MouseListener} {@link MouseWheelListener} but uses only
 * {@link KeyListener#keyTyped(KeyEvent)}, {@link KeyListener#keyPressed(KeyEvent)}, {@link MouseWheelListener#mouseWheelMoved(MouseWheelEvent)}
 */
public class ConsoleListener implements KeyListener, FocusListener, MouseListener, MouseWheelListener {
    Console console;
    String Blocking = null;

    public ConsoleListener(Console c) {
        console = c;
    }

    @Override
    public void focusGained(FocusEvent e) { }

    @Override
    public void focusLost(FocusEvent e) { }

    //Key-handling for the console.
    @Override
    public void keyTyped(KeyEvent e) {
        //If the user presses enter flush the input
        if (KeyEvent.VK_ENTER == e.getKeyChar())
            console.flushInput();
        //If the user presses delete remove the last charter(if there is one).
        else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE)
            console.removeLastFromTemp();
        //If another key is pressed and control is not pressed try adding a character to the console's input.
        else if (!e.isControlDown())
            console.addToTemp(e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.isControlDown()) {
            //Copy handling.
            if (e.getKeyCode() == KeyEvent.VK_C) {
                StringSelection selection = new StringSelection(console.getTemp());
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            } else if (e.getKeyCode() == KeyEvent.VK_V)
                //Paste handling.
                try {
                    if (Toolkit.getDefaultToolkit().getSystemClipboard().isDataFlavorAvailable(DataFlavor.stringFlavor))
                        console.addToTemp((String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            //Return one line backwards.
            console.getHistory().previous();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            //Return one line forwards.
            console.getHistory().next();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) { }

    @Override
    public void mouseClicked(MouseEvent e) { }

    @Override
    public void mousePressed(MouseEvent e) { }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }

    @Override
    public void mouseExited(MouseEvent e) { }

    //Handle scrolling the console.
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        switch (e.getWheelRotation()) {
            case -1:
                //Scroll up.
                if (console.getScroll() / 20 > e.getScrollAmount())
                    console.getHistory().scroll((-2) * e.getScrollAmount());
                break;
            case 1:
                //Scroll down.
                if (console.getScroll() / 20 < console.getLines().size() - Console.LineWrap)
                    console.getHistory().scroll(2 * e.getScrollAmount());
                break;
        }
    }
}