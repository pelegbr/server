package com.tos.server.console;

import com.tos.server.console.commands.ICommandExec;
import com.tos.server.console.history.ConsoleHistory;
import com.tos.server.console.history.IConsoleHistory;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.console.render.RenderEvent;
import com.tos.server.console.render.IRenderEventDispatcher;
import com.tos.server.console.util.ParserUtil;
import com.tos.server.util.StringUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This class is a full recreation of a system terminal/console mechanism.
 * It includes window, commands and IO
 * It also handles threading for it's own.
 */
public class Console extends Thread {
    //The manager of the console, handles commands, updating and sorting the consoles by UUID.
    private final ConsoleManager manager;
    //The console's window.
    private JFrame consoleWindow;
    //The Unique Id dedicated to this console.
    private final UUID uniqueId;
    //Is the console running currently.
    private boolean running;
    //The renderer for this console without this the console won't display anything.
    private final List<IRenderEventDispatcher> dispatchers;
    //The text buffer of the console.
    private List<String> lines;
    //The colour of the background of the console.
    private Color backgroundColour;

    //What String ends the blocking mode
    private String blocking = null;
    //A flag signaling whether the console is in blocking mode(awaiting input).
    private volatile boolean listening;

    //The object holding the history of this console.
    private volatile IConsoleHistory history;

    //The number of charters in a line(at most) before attempting to cut it.
    private int wordwrap = 85;
    //The number of lines in a console before it starts to scroll down.
    public static final int LineWrap = 28;

    //The default constructor.
    public Console() {
        this.manager = ConsoleManager.instance;
        uniqueId = ConsoleManager.next();
        consoleWindow = new JFrame();
        dispatchers = new ArrayList<>();
        lines = new ArrayList<>();
        history = new ConsoleHistory();
        backgroundColour = Color.black;
    }

    /**
     * @param uniqueId the Identifier that is unique to only this console.
     * @param Title    the title of the console window
     * @param manager  the manager of this console {@link #manager}
     */
    public Console(UUID uniqueId, String Title, ConsoleManager manager) {
        this.uniqueId = uniqueId;
        this.manager = manager;
        consoleWindow = new JFrame();
        consoleWindow.setTitle(Title);
        setName(Title + " console Thread");
        dispatchers = new ArrayList<>();
        lines = new ArrayList<>();
        history = new ConsoleHistory();
        backgroundColour = Color.black;
    }

    /**
     * This function fetches what is in the input of console currently.
     *
     * @return the current input text in the console.
     */
    public String getTemp() {
        return history.getCurrent().get();
    }

    /**
     * This function opens a dialog that allows you to chose the background colour of the console.
     */
    public void inputColour() {
        backgroundColour = JColorChooser.showDialog(null, "Select the background colour", Color.cyan);
        if (backgroundColour == null) backgroundColour = Color.BLACK;
    }

    /**
     * @return the unique identifier of the console.
     */
    public UUID getUniqueId() {
        return uniqueId;
    }

    /**
     * @return the text buffer of the console.
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * @return the scroll of the console.
     */
    public int getScroll() {
        return history.getScroll();
    }

    /**
     * @return the manager of the console.
     */
    public ConsoleManager getManager() {
        return manager;
    }

    /**
     * This function changes the behavior of console when the X button is pressed.
     *
     * @param closeBehavior the behavior wished to enact when the X button is pressed.
     * @return this
     */
    public Console setCloseBehavior(int closeBehavior) {
        consoleWindow.setDefaultCloseOperation(closeBehavior);
        return this;
    }

    /**
     * @param backgroundColour the new background colour for the console.
     */
    public void setBackgroundColour(Color backgroundColour) {
        this.backgroundColour = backgroundColour;
    }

    /**
     * @param wordwrap the new number of characters for line in the console.
     */
    public void setWordwrap(int wordwrap) {
        this.wordwrap = wordwrap;
    }

    /**
     * This function sets a line at lines[start]
     *
     * @param start the line number.
     * @param s     the string to add.
     */
    protected void setLine(int start, String s) {
        if (lines.size() == start) {
            //If after the last line add instead of set.
            if (s.length() > wordwrap)
                addLines(new String[]{s.substring(0, wordwrap), s.substring(wordwrap)});
            else
                lines.add(s);
        } else if (lines.size() > start) {
            //If bigger than wordwrap recurse to this function with s.substring(wordwrap) else just set the line.
            if (s.length() > wordwrap) {
                lines.set(start, s.substring(0, wordwrap));
                setLine(start + 1, s.substring(wordwrap));
            } else
                lines.set(start, s);
        } else
            //OutOfBounds index in the lines array.
            throw new IndexOutOfBoundsException("tried to set line of " + start + "when the line array is too short.");
    }

    /**
     * This function add new lines to the buffer.
     *
     * @param add the lines to add to the array.
     */
    protected void addLines(String[] add) {
        for (String line : add) {
            if (line.length() > wordwrap)
                addLines(new String[]{line.substring(0, wordwrap), line.substring(wordwrap)});
            else
                lines.add(line);
        }
    }

    /**
     * This function subscribes a {@link IRenderEventDispatcher} to this console.
     *
     * @param dispatcher the renderer.
     * @return this.
     */
    public Console addRenderEventDispatcher(IRenderEventDispatcher dispatcher) {
        dispatchers.add(dispatcher);
        return this;
    }

    /**
     * This function subscribes a {@link ConsoleListener} to this console.
     *
     * @param listener the listener.
     * @return this.
     */
    protected Console addConsoleListener(ConsoleListener listener) {
        //Subscribe to all window events, focus, key, mouse, and scroll.
        consoleWindow.addFocusListener(listener);
        consoleWindow.addKeyListener(listener);
        consoleWindow.addMouseListener(listener);
        consoleWindow.addMouseWheelListener(listener);
        return this;
    }

    /**
     * This function adds one character to the input field of the console.
     *
     * @param c the character to add to the input.
     */
    public void addToTemp(char c) {
        //If it's blocking satisfy the block if you can.
        if (blocking != null && blocking.isEmpty()) {
            listening = false;
            blocking = null;
        }
        //If the charater is legal for the console(can be displayed) add it to the input.
        if (consoleWindow.getFont().canDisplay(c))
            history.getCurrent().set(history.getCurrent().get() + c);
    }

    /**
     * This function adds multiple characters to the input field of the console.
     *
     * @param s the string to add to the input field.
     */
    public void addToTemp(String s) {
        history.getCurrent().set(history.getCurrent().get() + s);
    }

    /**
     * This function prints an object using it's {@link #toString()}
     *
     * @param o the object to print.
     */
    public void print(Object o) {
        //If the object is null or empty return.
        if (o == null || o.toString().isEmpty()) return;

        //If the string does not contain a colour information add to it the default one.
        String string = o.toString().startsWith("&@") ? o.toString() : "&@w" + o.toString();
        //Split the string by lineends.
        List<String> lines = StringUtil.split((this.lines.size() > 0 ? this.lines.get(this.lines.size() - 1) : "") + string, "\n");

        //Set the first line, this either adds to the last line if it didn't end with lineend or add a new one if it does.
        setLine(this.lines.size() > 0 ? this.lines.size() - 1 : 0, lines.get(0));
        //Add the rest of the text.
        this.lines.addAll(lines.subList(1, lines.size()));
        //Linewrap.
        if (this.lines.size() > LineWrap)
            history.scroll((this.lines.size() - LineWrap - history.getScroll() / 20) * 20);//scroll = lines.size() - LineWrap;
    }

    /**
     * This function prints an object using it's {@link #toString()} and add a newline at it's end.
     *
     * @param o the object to print.
     */
    public void println(Object o) {
        //If the object is null return.
        if (o == null) o = "null";

        //If the string does not contain a colour information add to it the default one.
        String string = o.toString().startsWith("&@") ? o.toString() : "&@w" + o.toString();
        //Split the string by lineends.
        List<String> lines = StringUtil.split((this.lines.size() > 0 ? this.lines.get(this.lines.size() - 1) : "") + string + "\n", "\n");

        //Set the first line, this either adds to the last line if it didn't end with lineend or add a new one if it does.
        setLine(this.lines.size() > 0 ? this.lines.size() - 1 : 0, lines.get(0));
        //Add the rest of the text.
        this.lines.addAll(lines.subList(1, lines.size()));
        //Linewrap.
        if (this.lines.size() > LineWrap)
            history.scroll((this.lines.size() - LineWrap - history.getScroll() / 20) * 20);//scroll = (lines.size() - LineWrap) * 20;
    }

    /**
     * This function blocks this thread({@link Thread#currentThread()} until the console receives a character input.
     *
     * @return the character entered to the console.
     */
    public char read() {
        while (listening) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Set the flag to blocking mode.
        listening = true;
        //Any character ends this blocking
        blocking = "";

        while (listening) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Get the last entered character.
        return history.getCurrent().get().charAt(history.getCurrent().get().length() - 1);
    }

    /**
     * This function blocks this thread({@link Thread#currentThread()} until the console receives a endline(enter) input.
     *
     * @return the line entered to the console.
     */
    public String readLine() {
        while (listening) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Set the flag to blocking mode.
        listening = true;
        //Only \n(lineend) character ends this blocking
        blocking = "\n";

        while (listening) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Get the current input push it in the history(since it cancels the event) and return it.
        String temp = history.getCurrent().get();
        history.push(true);

        return temp;
    }

    /**
     * This function kills a console.
     */
    public void Stop() {
        running = false;
        consoleWindow.dispose();
    }

    /**
     * This is the function that keeps the console updated and rendering.
     */
    @Override
    public void run() {
        running = true;
        //Setting up hte console window
        consoleWindow.setResizable(false);
        consoleWindow.setLocationRelativeTo(null);
        consoleWindow.setBounds(100, 100, 600, 600);
        consoleWindow.setLayout(new FlowLayout());
        consoleWindow.setVisible(true);
        consoleWindow.createBufferStrategy(3);
        consoleWindow.setIgnoreRepaint(true);

        //Add the default listener.
        addConsoleListener(new ConsoleListener(this));
        //Rendering setup.
        BufferStrategy strategy = consoleWindow.getBufferStrategy();

        while (running) {
            try {
                Thread.sleep(33, 333);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //Event invocation.
            render(strategy);
        }
    }

    /**
     * This function does a minimal paint of the background and invoke all renderers.
     *
     * @param strategy the BufferStartegy for painting in the console window
     */
    private void render(BufferStrategy strategy) {
        //Render Init.
        Graphics g = strategy.getDrawGraphics();
        g.setColor(backgroundColour);

        //Render. Events
        g.fillRect(0, 0, consoleWindow.getWidth(), consoleWindow.getHeight());
        for (IRenderEventDispatcher dispatcher : dispatchers) {
            if (dispatcher.OnRender(new RenderEvent(g, this)))
                break;
        }

        //Render Ending. cleanup
        g.dispose();
        strategy.show();
    }

    /**
     * This function pushes the current input in the history and executes a command when such is called by the input.
     */
    public void flushInput() {
        if (history.getHistory().size() > 0 && !history.getHistory().get(history.getHistory().size() - 1).equals(history.getCurrent().get()))
            history.getHistory().remove(history.getHistory().size() - 1);
        //Handle when blocking mode should be turned off.
        if (blocking != null && blocking.equals("\n")) {
            listening = false;
            blocking = null;
            while (!history.getCurrent().get().isEmpty()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            println(history.getHistory().get(history.getIndex()));
        } else {
            //Find and execute the requested command if one is available otherwise print an error.
            ICommandExec exec;
            List<Object> parameters = new ArrayList<>();
            if (history.getCurrent().get().contains("(") && history.getCurrent().get().contains("(")) {
                exec = ParserUtil.Parse(this, history.getCurrent().get(), parameters);
                if (exec == null)
                    println("unrecognized command " + history.getCurrent().get().substring(0, history.getCurrent().get().indexOf("(")));
                else {
                    Object execRet = exec.exec(this, parameters.toArray());
                    if (execRet != null)
                        println(execRet);
                }
            } else
                println(history.getCurrent().get());
            history.push(true);
        }
    }

    /**
     * @return the History Object.
     */
    protected IConsoleHistory getHistory() {
        return history;
    }

    /**
     * This function removes a character from the input field.
     */
    public void removeLastFromTemp() {
        if (history.getCurrent().get().length() > 0)
            history.getCurrent().set(history.getCurrent().get().substring(0, history.getCurrent().get().length() - 1));
    }

    /**
     * This function resets the console.
     */
    public void clear() {
        lines.clear();
        listening = false;
        blocking = null;
        history.clear(false);
    }
}