package com.tos.server.console.manager;

import com.tos.server.console.commands.Common.CommonCommand;
import com.tos.server.console.commands.ICommand;
import com.tos.server.console.commands.ICommandExec;
import com.tos.server.console.Console;
import com.tos.server.console.render.RenderEvenDispatcher;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * This class is a manager for the {@link Console} class, it determines what commands it has, what's it's UUID, colour info, and updating/starting/closing of the consoles under it.
 */
public class ConsoleManager {
    //UUID decision fields.
    static long ins = 20;
    static long sings = 0;
    //The consoles and their UUIDs
    protected Map<UUID, Console> consoles;
    //The main Console's UUID.
    protected UUID main;

    //The commands that a console may execute.
    protected Map<String, List<ICommandExec<?>>> commands;
    //A singleton, use this if you don't want to create a new one.
    public static ConsoleManager instance = new ConsoleManager(new UUID(sings, ins += 20));

    /**
     * This function gets a console by it's UUID
     * @param id the UUID of the console you wish to get.
     * @return the console matching the request UUID.
     */
    public Console get(UUID id) {
        return consoles.get(id);
    }

    /**
     * This function gets the main console of this manager.
     * @return the main console of this manager.
     */
    public Console getMain() {
        return consoles.get(main);
    }

    /**
     * This function Creates a new UUID for a console.
     * @return a new UUID for a console.
     */
    public static UUID next() {
        ins += 20;
        if (ins % 2000 == 0)
            sings++;
        return new UUID(sings, ins);
    }

    /**
     * This constructor takes a UUID for the main Console.
     * @param main the UUID for the main console.
     */
    public ConsoleManager(UUID main) {
        this.main = main;
        consoles = new HashMap<>();
        commands = new HashMap<>();
        //Add the main console.
        consoles.put(main, new Console(main, "main console", this).setCloseBehavior(WindowConstants.EXIT_ON_CLOSE).addRenderEventDispatcher(new RenderEvenDispatcher()));
        //Add the clear command.
        register("clear", new CommonCommand() {
            @Override
            public Object exec(Console console, Object... parameters) {
                console.clear();
                return null;
            }
        });
    }

    /**
     * This function stops all the Consoles under this ConsoleManager.
     */
    public void stop() {
        for (Console c : consoles.values())
            c.Stop();
    }

    /**
     * This function starts all the Consoles under this ConsoleManager.
     * @return this.
     */
    public ConsoleManager start() {
        for (Console c : consoles.values())
            if (!c.isAlive())
                c.start();
        return this;
    }

    /**
     * This function prints an object at the main Console.
     * @param o the object to print.
     */
    public void print(Object o) {
        consoles.get(main).print(o);
    }

    /**
     * This function prints an object and a newline at the main Console.
     * @param o the object to print.
     */
    public void println(Object o) {
        consoles.get(main).println(o);
    }

    /**
     * This function is a convenience function to print just an empty line to the main console.
     */
    public void println() {
        consoles.get(main).println("");
    }

    /**
     * This function reads a character from the main Console(blocks the current Thread until it receives a character input).
     * @return the character read.
     */
    public char read() {
        return consoles.get(main).read();
    }

    /**
     * This function reads a line from the main Console(blocks the current Thread until it receives an lineend/enter input).
     * @return the line read.
     */
    public String readLine() {
        if (consoles.get(main).isAlive())
            return consoles.get(main).readLine();
        return null;
    }


    /**
     * This function adds a console to this manager.
     * @param c the console to add to the managed consoles.
     */
    public void add(Console c) {
        consoles.put(c.getUniqueId(), c);
        if (!c.isAlive()) c.start();
    }

    /**
     * This function removes a console from this manager.
     * @param id the UUID of the remove console.
     */
    public void remove(UUID id) {
        if (id.equals(main)) throw new IllegalStateException("Can't Remove main console: console " + id);
        consoles.remove(id).Stop();
    }

    /**
     * This function registers commands given to it to this manager.
     * @param commands the map of commands(names and functionalities) to add.
     */
    public void register(Map<String, List<ICommand<?>>> commands) {
        for (String s : commands.keySet()) {
            List<ICommandExec<?>> i = this.commands.computeIfAbsent(s, k -> new ArrayList<>());
            for (ICommand<?> command : commands.get(s))
                i.add(command.getExec());
        }
    }

    /**
     * This function registers a command to this manager.
     * @param regName the name of the command to add.
     * @param command the functionality of the command.
     */
    public void register(String regName, ICommand command) {
        commands.computeIfAbsent(regName, k -> new ArrayList<>()).add(command.getExec());
    }

    /**
     * This function searchs for a command base on it's name and parameter types.
     * @param name the name of the command.
     * @param parameters the parameter types of the commands.
     * @return the found command that matches the details given.
     */
    public ICommandExec getCommand(String name, Class... parameters) {
        return commands.getOrDefault(name, Collections.emptyList()).stream().filter(exec -> exec.accepts(parameters)).findFirst().orElse(null);
    }

    /**
     * This function searchs for a command base on it's name and parameter types.
     * @param name the name of the command.
     * @param parameters the parameter of the commands.
     * @return the found command that matches the details given.
     */
    public ICommandExec getCommand(String name, Object... parameters) {
        return commands.getOrDefault(name, Collections.emptyList()).stream().filter(exec -> exec.accepts(parameters)).findFirst().orElse(null);
    }

    /**
     * This function calls a command with a given name and parameters.
     * @param name the name of the command.
     * @param parameters the parameters to call the command with.
     * @param <T> the return type of the command.
     * @return what the command returned.
     */
    @SuppressWarnings("unchecked")
    public <T> T exec(String name, Object... parameters) {
        //Find the command.
        ICommandExec<T> exec = getCommand(name, parameters);
        //If found call it.
        if (exec != null)
            return exec.exec(getMain(), parameters);
        return null;
    }

    /**
     * This function maps a character to a colour.
     * @param c the character that determines the colour.
     * @return the colour matching c.
     */
    public Color getMapped(char c) {
        switch (c) {
            case 'r': return Color.RED;
            case 'b': return Color.BLUE;
            case 'c': return Color.CYAN;
            case 'y': return Color.YELLOW;
            case 'g': return Color.GREEN;
            case 'm': return Color.MAGENTA;
            case 'p': return Color.PINK;
            case 'o': return Color.ORANGE;
        }
        return Color.WHITE;
    }
}