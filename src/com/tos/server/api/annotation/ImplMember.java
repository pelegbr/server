package com.tos.server.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
/**
 * This annotation marks that a class is a part of the core implementation of the Server.
 */
public @interface ImplMember {
    /**
     * @return what in what versions the class exists/is ImplMember.
     */
    String[] Versions();
}
