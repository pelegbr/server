package com.tos.server.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
/**
 * This annotation marks a class as a member of the core API of the server.
 */
public @interface ApiMember {
    /**
     * @return what versions that class exists/is ApiMember
     */
    String[] Versions();
}
