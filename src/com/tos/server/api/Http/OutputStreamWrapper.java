package com.tos.server.api.Http;

import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Consumer;

/**
 * This class is a buffered wrapper of {@link OutputStream}
 * It functions like a buffered version of it's {@link #stream}
 */
public class OutputStreamWrapper extends OutputStream {
    //The wrapped stream.
    private final OutputStream stream;
    //When the stream is closed/flushed an eventHandler is called with len as it's parameter.
    private final Consumer<Integer> onFlush;
    //The length of the used part of the buffer.
    private int len;
    //The place the data is stored in until flush is called.
    private byte[] buffer;

    /**
     * This constructor initializes buffer to a size of 4096.
     *
     * @param stream  the stream to wrap.
     * @param onFlush the event handler the flush event.
     */
    public OutputStreamWrapper(OutputStream stream, Consumer<Integer> onFlush) {
        buffer = new byte[4096];
        len = 0;
        this.stream = stream;
        this.onFlush = onFlush;
    }

    /**
     * Writes the specified byte to this output stream. The general
     * contract for <code>write</code> is that one byte is written
     * to the output stream. The byte to be written is the eight
     * low-order bits of the argument <code>b</code>. The 24
     * high-order bits of <code>b</code> are ignored.
     * <p>
     * Subclasses of <code>OutputStream</code> must provide an
     * implementation for this method.
     *
     * @param b the <code>byte</code>.
     * @throws IOException if an I/O error occurs. In particular,
     *                     an <code>IOException</code> may be thrown if the
     *                     output stream has been closed.
     */
    @Override
    public void write(int b) {
        buffer[len++] = (byte) b;
        if (len == buffer.length) {
            byte[] temp = buffer;
            buffer = new byte[temp.length + 4096];
            System.arraycopy(temp, 0, buffer, 0, temp.length);
        }
    }

    /**
     * Writes <code>b.length</code> bytes from the specified byte array
     * to this output stream. The general contract for <code>write(b)</code>
     * is that it should have exactly the same effect as the call
     * <code>write(b, 0, b.length)</code>.
     *
     * @param b the data.
     * @throws IOException if an I/O error occurs.
     * @see java.io.OutputStream#write(byte[], int, int)
     */
    @Override
    public void write(byte[] b) {
        write(b, 0, b.length);
    }


    /**
     * Writes <code>len</code> bytes from the specified byte array
     * starting at offset <code>off</code> to this output stream.
     * The general contract for <code>write(b, off, len)</code> is that
     * some of the bytes in the array <code>b</code> are written to the
     * output stream in order; element <code>b[off]</code> is the first
     * byte written and <code>b[off+len-1]</code> is the last byte written
     * by this operation.
     * <p>
     * The <code>write</code> method of <code>OutputStream</code> calls
     * the write method of one argument on each of the bytes to be
     * written out. Subclasses are encouraged to override this method and
     * provide a more efficient implementation.
     * <p>
     * If <code>b</code> is <code>null</code>, a
     * <code>NullPointerException</code> is thrown.
     * <p>
     * If <code>off</code> is negative, or <code>len</code> is negative, or
     * <code>off+len</code> is greater than the length of the array
     * <code>b</code>, then an <tt>IndexOutOfBoundsException</tt> is thrown.
     *
     * @param b   the data.
     * @param off the start offset in the data.
     * @param len the number of bytes to write.
     * @throws IOException if an I/O error occurs. In particular,
     *                     an <code>IOException</code> is thrown if the output
     *                     stream is closed.
     */
    @Override
    public void write(byte[] b, int off, int len) {
        if (this.len + len > buffer.length) {
            byte[] temp = buffer;
            buffer = new byte[((this.len + len) / 4096 + 1) * 4096];
            System.arraycopy(temp, 0, buffer, 0, this.len);
        }
        System.arraycopy(b, off, buffer, this.len, len);
        this.len += len;
    }

    /**
     * Flushes this output stream and forces any buffered output bytes
     * to be written out. The general contract of <code>flush</code> is
     * that calling it is an indication that, if any bytes previously
     * written have been buffered by the implementation of the output
     * stream, such bytes should immediately be written to their
     * intended destination.
     * <p>
     * In this case the wrapped stream,
     * afterwards an onFlush event is called and the stream is restarted.
     * </p>
     *
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void flush() throws IOException {
        onFlush.accept(len);
        stream.write(buffer, 0, len);
        buffer = null;
        len = -1;
        stream.flush();
    }


    /**
     * Closes this output stream and releases any system resources
     * associated with this stream. The general contract of <code>close</code>
     * is that it closes the output stream. A closed stream cannot perform
     * output operations and cannot be reopened.
     * <p>
     * The <code>close</code> method of <code>OutputStream</code> does nothing.
     *
     * @throws IOException if an I/O error occurs.
     */
    @Override
    public void close() throws IOException {
        if (buffer != null) flush();
        stream.close();
    }
}
