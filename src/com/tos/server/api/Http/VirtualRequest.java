package com.tos.server.api.Http;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.api.annotation.ApiMember;
import com.tos.server.Statics;
import com.tos.server.util.HttpUtil;
import com.tos.server.util.SerialUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.UUID;

@ApiMember(Versions = Statics.ApiVersion)
/**
 * This class was a part of a rewrite that wasn't complete.
 */
public abstract class VirtualRequest {
    private boolean handled = false;
    public final byte[] data;
    private final Map<String, String> parameters;
    //will be excluded from data but will always be sent in POST.
    private UUID token;
    public final String method;

    public UUID getToken() {
        String token;
        if (data[0] != 'l') {
//            token = new String(Statics.encryptor.enc(data), 1, 36);
//            Date d = DBUtil.getTokenExpirationDate(token);
//            if (d.before(new Date(System.currentTimeMillis())))
//                return null;
//            else
//                this.token = UUID.fromString(token);
//            this.token = UUID.fromString(new String(Statics.encryptor.dec(token.getBytes())));
        } else {
//            String username = parameters.get("Username");
//            String password = parameters.get("Password");
//            if (username == null || password == null) return null;
//            this.token = UUID.randomUUID();
//            DBUtil.ensureNotTaken(this.token);
//            DBUtil.writeToken(this.token.toString(), new Date(System.currentTimeMillis() + 30 * 24 * 60 * 60 * 1000L), username, password);
        }
        return this.token;
    }

    protected abstract OutputStream init(int responseCode, long responseLength) throws IOException;

    public VirtualRequest(String quarry, String type, InputStream in) throws IOException {
//        byte[] buffer = new byte[4096];
//        while (in.read(buffer, buffer.length - 4096, 4096) != -1) {
//            byte[] b = buffer;
//            buffer = new byte[b.length + 4096];
//            System.arraycopy(b, 0, buffer, 0, b.length);
//        }

        data = SerialUtil.readStream(in,true);
//        in.close();
//        data = SerialUtil.trimBytes(buffer);

        this.method = type;
        parameters = HttpUtil.getParameters(quarry);
    }


    public OutputStream handle(int responseCode, long responseLength) throws IOException {
        handled = true;
        return init(responseCode, responseLength);
    }

    public boolean isHandled() {
        return handled;
    }

    public static VirtualRequest fromExchange(HttpExchange exchange) throws IOException {
        return new VirtualRequest(exchange.getRequestURI().getQuery(), exchange.getRequestMethod(), exchange.getRequestBody()) {
            @Override
            public OutputStream init(int responseCode, long responseLength) throws IOException {
                exchange.sendResponseHeaders(responseCode, responseLength);
                return exchange.getResponseBody();
            }
        };
    }
}
