package com.tos.server.api.Http;

import com.sun.net.httpserver.HttpExchange;

/**
 * This class represents a RequestMethod either HTTP::POST or HTTP:GET.
 */
public enum RequestType {
    GET, POST;

    /**
     * This function is a utility for getting a request type from {@link HttpExchange}
     * @param exchange the request data.
     * @return the request type of the request.
     */
    public static RequestType get(HttpExchange exchange) {
        return valueOf(exchange.getRequestMethod());
    }
}
