package com.tos.server.api.Http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.tos.server.api.annotation.ApiMember;
import com.tos.server.api.annotation.OnFail;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.provider.PageProvider;
import com.tos.server.provider.ResponseProvider;
import com.tos.server.provider.fallbackProvider;
import com.tos.server.Statics;
import com.tos.server.util.HttpUtil;
import com.tos.server.util.SerialUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@ApiMember(Versions = Statics.ApiVersion)
/**
 * This class is the main handler of the server, it gets http requests and delegates it to the right provider.1
 */
public class VirtualHandler {
    //Fatal error text.
    public static final String FALLBACK_FAILED = "The Server Files have been modified/removed.as a result it couldn't handle a request please refer to the code manufacturer for more information.";
    //The providers that answer the requests.
    private final List<ResponseProvider> providers;
    //The subscribers to failure see OnFail
    private final List<Method> eventHandlers;
    //The last provider before a fatal error.
    private static final ResponseProvider fallback = new fallbackProvider();

    /**
     * @return a Virtual HttpHandler delegating to {@link #handle(HttpExchange)}
     */
    public HttpHandler Create() {
        return this::handle;
    }

    /**
     * This is a constructor that takes the providers.
     * @param responseProviders the providers used by this handler.
     */
    public VirtualHandler(ResponseProvider... responseProviders) {
        providers = new ArrayList<>();
        eventHandlers = new ArrayList<>();

        providers.addAll(responseProviders.length == 0 ? Collections.singleton(new PageProvider()) : Arrays.asList(responseProviders));
    }

    /**
     * This function adds an event handler for onFail.
     * @param c the class containing the method handling the failure, the method has to be static.
     */
    public void registerEventHandler(Class c) {
        for (Method method : c.getDeclaredMethods()) {
            if (method.isAnnotationPresent(OnFail.class)) {
                if (!eventHandlers.contains(method))
                    eventHandlers.add(method);
            }
        }
    }

    /**
     * This method notifies all handlers of the failure.
     * @param provider the provider failing to handle a request.
     */
    public void onFail(ResponseProvider provider) {
        for (Method m : eventHandlers) {
            try {
                m.invoke(provider);
            } catch (Exception e) {
                Fatal("Couldn't call an event handler : " + m + ".", "Server shutting down.");
            }
        }
    }

    /**
     * This function where the request to the server are routed, from here delegated to the right provider.
     * @param httpExchange the requests data.
     */
    private void handle(HttpExchange httpExchange) {
        try {
            RequestType type = RequestType.get(httpExchange);
            //Read post data.
            byte[] data = type == RequestType.POST ? SerialUtil.readStream(httpExchange.getRequestBody(), true) : new byte[0];
            //Get request type.
            //Check what provider can handle this request and call it.
            for (ResponseProvider provider : providers) {
                if (provider.canHandle(type)) {
                    if (provider.tryServe(httpExchange.getRequestURI().getPath(), httpExchange, data)) {
                        return;
                    }
                    //If the provider failed notify event handlers.
                    onFail(provider);
                }
            }
            if (!fallback.tryServe(httpExchange.getRequestURI().getQuery(), httpExchange, data))
                Fatal(FALLBACK_FAILED, "Fallback failed Closing Server.");
        } catch (Exception e) {
            ConsoleManager.instance.println(e);
            Arrays.asList(e.getStackTrace()).forEach(te -> ConsoleManager.instance.println("&@r" + te));
        }
    }

//Unfinished rewrite
//    private void handle(HttpExchange httpExchange) {
//        try {
//            VirtualRequest request = VirtualRequest.fromExchange(httpExchange);
//            for (ResponseProvider provider : providers) {
//                if (provider.handles(request)) {
//                    provider.serve(request);
//                    if (request.isHandled())
//                        return;
//                    onFail(provider);
//                }
//            }
//
//            fallback.serve(request);
//            if (!request.isHandled())
//                Fatal(FALLBACK_FAILED, "Fallback failed Closing Server.");
//        } catch (Exception e) {
//            ConsoleManager.instance.println(e);
//            Arrays.asList(e.getStackTrace()).forEach(te -> ConsoleManager.instance.println("&@r" + te));
//        }
//    }

    /**
     * This function is for when the server hit a fatal state and needs to close.
     * @param exMessage the exception details.
     * @param message the message to display before closing.
     */
    public static void Fatal(String exMessage, String message) {
        //exception printing.
        ConsoleManager.instance.println("&@r<Fatal>:" + exMessage);
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        for (int i = stackTrace.length - 1; i > 0; i--) {
            if (!"Fatal".equals(stackTrace[i].getMethodName()))
                ConsoleManager.instance.println("&@r" + stackTrace[i]);
        }
        ConsoleManager.instance.println("&@r" + message);

        //TODO . , .. , ... sequence.
        //Something like manager.animateStringLn("press any key to exit","...");
        //Notify the user to input to close the server.
        ConsoleManager.instance.println("press any key to exit...");

        //TODO: fix read to read any input...
        //Await user input.
        ConsoleManager.instance.read();
        //Exit.
        System.exit(2);
    }
}