package com.tos.server.api.Html;

import com.tos.server.Statics;
import com.tos.server.api.annotation.ApiMember;
import javafx.util.Pair;

import java.util.*;

@ApiMember(Versions = Statics.ApiVersion)
/**
 * This is a basic implementation of {@link HTMLComponent}.
 * Every type of html tag can be represented by it.
 */
public class JHTMLComponent implements HTMLComponent {
    //Parent component.
    protected HTMLComponent parent;
    //The tag name
    protected final String HtmlName;
    //The attribute map.
    private Map<String, String> attributes;
    //The components under this component.
    protected List<HTMLComponent> subComponents;
    //The textNodes.
    protected Map<Integer, String> text;
    //number of subnodes
    protected int nodes;
    //Can it have children
    private boolean emptyTag;
    //Can it have attributes.
    private boolean attributable;

    /**
     * This is the full constructor for a components.
     *
     * @param parent       the parent of this component.
     * @param HtmlName     the tag name.
     * @param emptyTag     what type of tag it with content or empty(can not have children too).
     * @param attributable whether the component can have attributes.
     * @param children     whether the component can have children.
     */
    public JHTMLComponent(HTMLComponent parent, String HtmlName, boolean emptyTag, boolean attributable, boolean children) {
        this.emptyTag = emptyTag;
        this.attributable = attributable;
        this.HtmlName = HtmlName;
        if (attributable)
            attributes = new HashMap<>();
        children &= !emptyTag;
        subComponents = children ? new ArrayList<>() : Collections.EMPTY_LIST;
        text = children ? new HashMap<>() : Collections.EMPTY_MAP;
    }

    /**
     * This is the minimal constructor for a component.
     *
     * @param HtmlName the tag name
     */
    public JHTMLComponent(String HtmlName) {
        this(null, HtmlName, false, true, true);
    }

    /**
     * This is an additional constructor for adding attributes at the creation of the component.
     *
     * @param parent     the parent component.
     * @param HtmlName   the tag name.
     * @param HtmlName   the tag name.
     * @param emptyTag   what type of tag it with content or empty(can not have children too).
     * @param children   whether the component can have children.
     * @param attributes the attributes of the component.
     */
    @SafeVarargs
    public JHTMLComponent(HTMLComponent parent, String HtmlName, boolean emptyTag, boolean children, Pair<String, String>... attributes) {
        this(parent, HtmlName, emptyTag, true, children);
        for (Pair<String, String> e : attributes) {
            setAttribute(e.getKey(), e.getValue());
        }
    }

    /**
     * This is an additional constructor for adding attributes at the creation of the component.
     *
     * @param parent     the parent component.
     * @param HtmlName   the tag name.
     * @param HtmlName   the tag name.
     * @param emptyTag   what type of tag it with content or empty(can not have children too).
     * @param children   whether the component can have children.
     * @param attributes the attributes of the component.
     */
    public JHTMLComponent(HTMLComponent parent, String HtmlName, boolean emptyTag, boolean children, Map<String, String> attributes) {
        this(parent, HtmlName, emptyTag, true, children);
        attributes.putAll(attributes);
    }

    /**
     * @return the subComponents of this component.
     */
    @Override
    public List<HTMLComponent> getSubComponents() {
        return subComponents;
    }

    /**
     * @return the parent of this component.
     */
    @Override
    public HTMLComponent getParent() {
        return parent;
    }

    /**
     * @return the text inside this component.
     */
    @Override
    public String innerHtml() {
        StringBuilder innerHtml = new StringBuilder();
        Iterator<HTMLComponent> components = getSubComponents().iterator();
        for (int i = 0; i < nodes; i++) {
            String s = text.get(i);
            //If string just add it to the builder.
            if (s != null)
                innerHtml.append(s).append('\n');
            else {
                //Else get the next component split it's lines and add all of them to the builder.
                String[] lines = components.next().toString().split("\n");
                innerHtml.append(lines[0]);
                //Spacing.
                if (lines.length > 1) {
                    for (int j = 1; j < lines.length - 1; j++) {
                        innerHtml.append("\n\t").append(lines[i]);
                    }
                    innerHtml.append('\n').append(lines[lines.length - 1]);
                }
            }
        }
        return innerHtml.toString();
    }


    /**
     * This function retrieves the internal text in component and decides it by lines.
     *
     * @return the lines inside the component.
     */
    @Override
    public String[] innerHtmlLines() {
        List<String> innerHtml = new ArrayList<>();
        Iterator<HTMLComponent> components = getSubComponents().iterator();
        for (int i = 0; i < nodes; i++) {
            String s = text.get(i);
            //If string just add it to the builder.
            if (s != null)
                innerHtml.add(s);
            else {
                //Else get the next component split it's lines and add all of them to the builder.
                String[] lines = components.next().toString().split("\n");
                innerHtml.add(lines[0]);
                //Spacing
                if (lines.length > 1) {
                    for (int j = 1; j < lines.length - 1; j++) {
                        innerHtml.add("\t" + lines[i]);
                    }
                    innerHtml.add(lines[lines.length - 1]);
                }
            }
        }
        return innerHtml.toArray(new String[0]);
    }

    /**
     * @return the name of this component's type.
     */
    public String getHtmlName() {
        return HtmlName;
    }

    /**
     * This function allows iteration on the subComponents of the component/
     *
     * @return an iterator for the subComponents of this Component.
     */
    @Override
    public Iterator<HTMLComponent> iterator() {
        return getSubComponents().iterator();
    }

    /**
     * This deserializes the data inside this component into html string.
     *
     * @return an html string.
     */
    @Override
    public String toString() {
        //Open the tag.
        StringBuilder html = new StringBuilder('<' + HtmlName);
        //Append all the attributes to the tag.
        for (Map.Entry<String, String> p : getAttributes().entrySet()) {
            if (!p.getKey().isEmpty())
                html.append(' ').append(p.getKey()).append("=\"").append(p.getValue()).append('\"');
        }
        //Close if empty tag
        if (emptyTag) html.append("/>");
        else {
            //Append children.
            html.append(">\n");
            String[] inner = innerHtmlLines();
            //Spacing.
            for (String s : inner) {
                html.append('\t').append(s).append('\n');
            }
            //Closing tag.
            html.append("</").append(HtmlName).append('>');
        }
        return html.toString();
    }


    /**
     * @return the attribute map.
     */
    @Override
    public Map<String, String> getAttributes() {
        return attributable ? attributes : Collections.emptyMap();
    }

    /**
     * This function sets a component's parent.
     *
     * @param parent the parent component.
     */
    @Override
    public void setParent(HTMLComponent parent) {
        this.parent = parent;
    }

    /**
     * This function adds an attribute to this component.
     *
     * @param key   the name of the attribute to add.
     * @param value the value of the attribute to add.
     * @return this.
     */
    @Override
    public HTMLComponent setAttribute(String key, String value) {
        if (attributable)
            attributes.put(key, value);
        return this;
    }

    /**
     * This function attempts to remove an element in {@link #getSubComponents()} and their {@link #getSubComponents()}.
     *
     * @param component the element to remove.
     * @return whether an element was removed or not.
     */
    @Override
    public boolean remove(HTMLComponent component) {
        List<HTMLComponent> components = getSubComponents();
        for (int i = 0; i < getSubComponents().size(); i++) {
            if (components.get(i).equals(component)) {
                components.remove(i);
                return true;
            } else if (components.get(i).remove(component))
                return true;
        }
        return false;
    }

    /**
     * This function search {@link #getSubComponents()} their {@link #getSubComponents()} and 'this' to find the requested component.
     *
     * @param component the component to search for.
     * @return if the component is found true, otherwise false.
     */
    @Override
    public boolean contains(HTMLComponent component) {
        for (HTMLComponent comp : getSubComponents()) {
            if (comp == component || comp.contains(component)) return true;
        }
        return false;
    }

    /**
     * This function removes this element from it's parent element.
     */
    @Override
    public void remove() {
        getParent().remove(this);
    }

    /**
     * This function appends a component to {@link #getSubComponents()} in the index specified.
     *
     * @param index     the place of the component.
     * @param component the component to append.
     */
    @Override
    public void append(int index, HTMLComponent component) {
        getSubComponents().add(index, component);
    }

    /**
     * This function appends a component to {@link #getSubComponents()} at the last place(if it the component supports it).
     *
     * @param component the component to append.
     */
    @Override
    public void append(HTMLComponent component) {
        if (getSubComponents() != Collections.EMPTY_LIST)
            getSubComponents().add(component);
        nodes++;
    }

    /**
     * This function adds a text node as a child of the component.
     *
     * @param text the text to add.
     */
    @Override
    public void append(String text) {
        this.text.put(nodes++, text);
    }

    /**
     * This function appends a component after this one.
     *
     * @param component the component to append.
     */
    @Override
    public void appendAfter(HTMLComponent component) {
        parent.append(parent.getSubComponents().indexOf(this) + 1, component);
    }

    /**
     * This function appends a component before this one.
     *
     * @param component the component to append.
     */
    @Override
    public void appendBefore(HTMLComponent component) {
        parent.append(parent.getSubComponents().indexOf(this) - 1, component);
    }


    /**
     * This function removes an attribute with a name matching 'key'
     *
     * @param key the name of the attribute to remove.
     * @return this.
     */
    @Override
    public HTMLComponent removeAttribute(String key) {
        if (attributable)
            attributes.remove(key);
        return this;
    }

    /**
     * Override equals and hash code for Collection use.
     * @return an integer representing the content of the component.
     */
    @Override
    public int hashCode() {
        //Name hash
        int hash = getHtmlName().hashCode();
        //Children hash
        for (HTMLComponent component : getSubComponents())
            hash += component.hashCode() >> 2;
        //Texts hash
        for (String s : text.values())
            hash += s.hashCode() >> 2;
        //Attribute hash
        hash += attributes.hashCode();
        return hash;
    }


    /**
     * Override equals and hash code for Collection use.
     * @param obj the object to equate the component to.
     * @return the equality to obj.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HTMLComponent) {
            HTMLComponent component = (HTMLComponent) obj;
            return component.getHtmlName().equals(getHtmlName()) && component.getParent() == getParent() && component.getAttributes().equals(getAttributes()) && component.getSubComponents().equals(subComponents);
        }
        return false;
    }
}
