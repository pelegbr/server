package com.tos.server.api.Html;

import com.tos.server.api.annotation.ApiMember;
import com.tos.server.Statics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ApiMember(Versions = Statics.ApiVersion)
/**
 * This class automates the creation of {@link HTMLDocument}.
 */
public class HtmlBuilder {
    //The internal components to the to the document when creating it.
    private List<Object> HTMLComponents;

    /**
     * This function adds a component to the document being built.
     * @param o the component(Could be text too) to add.
     * @return this.
     */
    public HtmlBuilder add(Object o) {
        if (o instanceof HTMLDocument)
            throw new RuntimeException("Can't recursively put a HtmlDocument Inside a HtmlDocument ");
        HTMLComponents.add(o);
        return this;
    }

    /**
     * This function removes a component from the document being built.
     * @param o the component(Could be text too) to remove.
     * @return this.
     */
    public HtmlBuilder remove(Object o) {
        HTMLComponents.remove(o);
        return this;
    }

    /**
     * This function adds a component to the document being built.
     * @param name the name of the attribute to add.
     * @param value the value of the attribute to add.
     * @return this.
     */
    public HtmlBuilder removeByAttribute(String name, String value) {
        HTMLComponents.remove(getByAttribute(name, value));
        return this;
    }

    /**
     * This constructor takes component and adds them to a document.
     * @param components the components to add.
     */
    public HtmlBuilder(HTMLComponent... components) {
        HTMLComponents = new ArrayList<>();
        if (components != null)
            Collections.addAll(HTMLComponents, components);
    }

    /**
     * This function returns the indexOf the component with this attribute.
     * @param name the name of the attribute to check.
     * @param value the value the attribute should have.
     * @return this
     */
    public int getByAttribute(String name, String value) {
        for (int i = 0; i < HTMLComponents.size(); i++) {
            if (HTMLComponents.get(i) instanceof HTMLComponent) {
                if (((HTMLComponent) HTMLComponents.get(i)).getAttributes().get(name).equalsIgnoreCase(value)) return i;
            }
        }
        return -1;
    }

    /**
     * This function finds the first component of a class.
     * @param cls the class you wish the component to be.
     * @return this.
     */
    public HTMLComponent get(String cls) {
        int index = getByAttribute("class", cls);
        return index == -1 ? null : (HTMLComponent) HTMLComponents.get(index);
    }

    /**
     * This function adds a component to the document being built.
     * @param attrName the name of the attribute to check in the component.
     * @param value the damned value of the attribute in the component.
     * @return this.
     */
    public HTMLComponent get(String attrName, String value) {
        return (HTMLComponent) HTMLComponents.get(getByAttribute(attrName, value));
    }

    /**
     * this function builds an {@link HTMLDocument} from the data given to the builder.
     * @return
     */
    public HTMLDocument build() {
        return new HTMLDocument(HTMLComponents.toArray());
    }

    /**
     * @return the html string of the component added so far.
     */
    @Override
    public String toString() {
        return build().toString();
    }
}
