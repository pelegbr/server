package com.tos.server.api.Html;

/**
 * This class represents a page inside the website linked to a file and is used in combination with {@link com.tos.server.provider.PageProvider}.
 */
public class Page {
    //The name of the page(the query required to navigate to it).
    private String path;
    //The location to the file of the page.
    private String filePath;

    /**
     * @param path the name of the page.
     * @param filePath the location of the file the page loads.
     */
    public Page(String path, String filePath) {
        this.path = path;
        this.filePath = filePath;
    }

    /**
     * @return the location the page loads.
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     *
     * @return the name of the page.
     */
    public String getPath() {
        return path;
    }
}
