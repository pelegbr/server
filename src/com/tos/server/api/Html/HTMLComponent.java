package com.tos.server.api.Html;

import com.tos.server.api.annotation.ApiMember;
import com.tos.server.Statics;

import java.util.List;

import java.util.Map;

@ApiMember(Versions = Statics.ApiVersion)
/**
 * This class is the baseline of all html components.
 */
public interface HTMLComponent extends Iterable<HTMLComponent> {
    /**
     * @return the subComponents of this component.
     */
    List<HTMLComponent> getSubComponents();

    /**
     * @return the parent of this component.
     */
    HTMLComponent getParent();

    /**
     * @return the text inside this component.
     */
    String innerHtml();

    /**
     * This function retrieves the internal text in component and decides it by lines.
     *
     * @return the lines inside the component.
     */
    String[] innerHtmlLines();

    /**
     * @return the name of this component's type.
     */
    String getHtmlName();

    /**
     * This function removes an attribute with a name matching 'key'
     *
     * @param key the name of the attribute to remove.
     * @return this.
     */
    HTMLComponent removeAttribute(String key);

    /**
     * This function adds an attribute to this component.
     *
     * @param key   the name of the attribute to add.
     * @param value the value of the attribute to add.
     * @return this.
     */
    HTMLComponent setAttribute(String key, String value);

    /**
     * This function search {@link #getSubComponents()} their {@link #getSubComponents()} and 'this' to find the requested component.
     *
     * @param component the component to search for.
     * @return if the component is found true, otherwise false.
     */
    boolean contains(HTMLComponent component);

    /**
     * This function attempts to remove an element in {@link #getSubComponents()} and their {@link #getSubComponents()}.
     *
     * @param component the element to remove.
     * @return whether an element was removed or not.
     */
    boolean remove(HTMLComponent component);

    /**
     * This function removes this element from it's parent element.
     */
    void remove();

    /**
     * This function appends a component to {@link #getSubComponents()} in the index specified.
     *
     * @param index     the place of the component.
     * @param component the component to append.
     */
    void append(int index, HTMLComponent component);

    /**
     * This function appends a component to {@link #getSubComponents()}.
     *
     * @param component the component to append.
     */
    void append(HTMLComponent component);

    /**
     * This function adds a text node as a child of the component.
     *
     * @param text the text to add.
     */
    void append(String text);

    /**
     * This function appends a component after this one.
     *
     * @param component the component to append.
     */
    void appendAfter(HTMLComponent component);

    /**
     * This function appends a component before this one.
     *
     * @param component the component to append.
     */
    void appendBefore(HTMLComponent component);

    /**
     * @return the attribute map.
     */
    Map<String, String> getAttributes();

    /**
     * This function sets a component's parent.
     * @param htmlComponents the parent component.
     */
    void setParent(HTMLComponent htmlComponents);
}
