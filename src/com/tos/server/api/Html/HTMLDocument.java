package com.tos.server.api.Html;

import com.tos.server.Statics;
import com.tos.server.api.annotation.ApiMember;

import java.util.*;

@ApiMember(Versions = Statics.ApiVersion)
public class HTMLDocument extends JHTMLComponent {
    /**
     * This constructor take the children of this component, either {@link String} or {@link HTMLComponent}
     *
     * @param elements the sub components(Strings or HtmlComponents).
     */
    public HTMLDocument(Object... elements) {
        //Initiate a component without any parent, full tag, does not allow attributes and allows children.
        super(null, "html", false, false, true);
        text = new HashMap<>();
        nodes = 0;
        //Add Strings to text and components to subComponents
        for (Object o : elements) {
            if (o instanceof HTMLComponent) {
                HTMLComponent component = (HTMLComponent) o;
                component.setParent(this);
                subComponents.add(component);
            } else
                text.put(nodes, (String) o);
            //Increase the count of elements.
            nodes++;
        }
    }

    /**
     * Disable the remove since {@link #parent) is always null
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    /**
     * This function searches in this document(recursive) for an element with a given id(@param id).
     *
     * @param id the id of the wanted Component.
     * @return the first component with this class or null if none is found.
     */
    public HTMLComponent getById(String id) {
        //For recursive search results.
        List<HTMLComponent> componentList = new ArrayList<>();
        //loop over all elements in subComponents
        for (HTMLComponent component : subComponents) {
            //If the component's id is (@param id) then return it.
            if (component.getAttributes().get("id").equals(id))
                return component;
            else {
                //Recursive search.
                getById(component.getSubComponents(), componentList, id, true);
                //If any component was found.
                if (!componentList.isEmpty()) return componentList.get(0);
            }
        }
        return null;
    }

    /**
     * This function searches in this document(recursive) for an element with a given class(@param cls).
     *
     * @param cls the class that an element should be of.
     * @return the first component with this class or null if none is found.
     */
    public HTMLComponent getByClass(String cls) {
        //For recursive search results.
        List<HTMLComponent> componentList = new ArrayList<>();
        //loop over all elements in subComponents
        for (HTMLComponent component : subComponents) {
            //If the component's class is (@param cls) then return it.
            if (component.getAttributes().get("class").equals(cls))
                return component;
            else {
                //Recursive search.
                getByClass(component.getSubComponents(), componentList, cls, true);
                //If any component was found.
                if (!componentList.isEmpty()) return componentList.get(0);
            }
        }
        return null;
    }

    /**
     * This function searches in the document(recursive) for an element with an attribute (@param attr) and a value (@param value) matching the ones given.
     *
     * @param attr  the attribute's name.
     * @param value the required value.
     * @return the element that has an attribute with the given value.
     */
    public HTMLComponent getByAttributeValue(String attr, String value) {
        //Storage of results
        List<HTMLComponent> results = new ArrayList<>(1);
        //Search(recursive)
        getByAttributeValue(subComponents, results, attr, value, true);
        //First element.
        return results.get(0);
    }

    /**
     * This function searches in this document(recursive) for an element with an attribute specified (@param attr).
     *
     * @param attr the attribute that an element should contain.
     * @return the first element with this attribute.
     */
    public HTMLComponent getByAttribute(String attr) {
        List<HTMLComponent> ret = new ArrayList<>(1);
        getByAttribute(subComponents, ret, attr, true);
        return ret.get(0);
    }

    /**
     * This function searches a list(@param components) for a component that has a certain type(@param type) and contained directly by another type(@param containing) and inserts them to (@param list).
     *
     * @param components the components to search(recursive).
     * @param list       the list of found components matching the criteria.
     * @param containing the type of the containing component.
     * @param type       the type of the component you're looking for.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getByTypeInsideType(List<HTMLComponent> components, List<HTMLComponent> list, String containing, String type, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //Check name equality
            if (sub.getHtmlName().equals(containing)) {
                //Check if one of the children is of (@param type)
                for (HTMLComponent com : sub.getSubComponents())
                    if (com.getHtmlName().equals(type))
                        list.add(com);
                //Check if should return, in case oneOnly is true.
                if (oneOnly && !list.isEmpty()) return;
            } else {
                //Recurse.
                getByTypeInsideType(sub.getSubComponents(), list, containing, type, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }


    /**
     * This function searches a list(@param components) for an element of a specified type(@param type) another element of another specified type(@param after) and inserts them to (@param list).
     *
     * @param components the components to search(recursive).
     * @param list       the list of found components matching the criteria.
     * @param after      the type of the component after the one you're looking for.
     * @param type       the type of the component you're looking for.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getByTypeAfterType(List<HTMLComponent> components, List<HTMLComponent> list, String after, String type, boolean oneOnly) {
        for (int i = 0; i < components.size(); i++) {
            //The current component to check.
            HTMLComponent sub = components.get(i);
            //Check if the type of the component is type & the next
            if (type.equals(sub.getHtmlName()) && components.size() > i + 1 && components.get(i + 1).getHtmlName().equals(after)) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse.
                getByTypeAfterType(sub.getSubComponents(), list, after, type, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }

    /**
     * This function Searches a list for elements of given types(@param types) and inserts it to (@param list).
     *
     * @param components the components to search for(recursive).
     * @param list       the list of found components matching the criteria.
     * @param types      the types of components to search for.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getByTypes(List<HTMLComponent> components, List<HTMLComponent> list, List<String> types, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //If the name is equal add to list.
            if (types.contains(sub.getHtmlName())) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse
                getByTypes(sub.getSubComponents(), list, types, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }


    /**
     * This function Searches a list for elements of given types(@param types) and inserts it to (@param list).
     *
     * @param components the components to search for(recursive).
     * @param list       the list of found components matching the criteria.
     * @param attr       attribute to search an element containing.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getByAttribute(List<HTMLComponent> components, List<HTMLComponent> list, String attr, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //Check if this component matches the attribute
            if (sub.getAttributes().containsKey(attr)) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse.
                getByAttribute(sub.getSubComponents(), list, attr, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }

    /**
     * This function Searches a list for elements of given types(@param types) and inserts it to (@param list).
     *
     * @param components the components to search for(recursive).
     * @param list       the list of found components matching the criteria.
     * @param attr       attribute to search an element containing.
     * @param value      the value of the attribute to search for an element containing.
     * @param oneOnly    whether to return after one component was found.
     */
    public void getByAttributeValue(List<HTMLComponent> components, List<HTMLComponent> list, String attr, String value, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //Check if this component matches the attribute
            if (value.equals(sub.getAttributes().get(attr))) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse.
                getByAttributeValue(sub.getSubComponents(), list, attr, value, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }

    /**
     * This function Searches a list for elements of given types(@param types) and inserts it to (@param list).
     *
     * @param components the components to search for(recursive).
     * @param list       the list of found components matching the criteria.
     * @param id         id to search an element possessing.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getById(List<HTMLComponent> components, List<HTMLComponent> list, String id, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //Check if this component matches the id
            if (id.equals(sub.getAttributes().get("id"))) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse
                getById(sub.getSubComponents(), list, id, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }


    /**
     * This function Searches a list for elements of given types(@param types) and inserts it to (@param list).
     *
     * @param components the components to search for(recursive).
     * @param list       the list of found components matching the criteria.
     * @param cls        class of the element to search.
     * @param oneOnly    whether to return after one component was found.
     */
    private void getByClass(List<HTMLComponent> components, List<HTMLComponent> list, String cls, boolean oneOnly) {
        for (HTMLComponent sub : components) {
            //Check if this component matches the class
            if (sub.getAttributes().get("class").equals(cls)) {
                list.add(sub);
                //Check if should return, in case oneOnly is true.
                if (oneOnly) return;
            } else {
                //Recurse
                getByClass(sub.getSubComponents(), list, cls, oneOnly);
                //If one and oneOnly is true is found fold the recursion.
                if (oneOnly && !list.isEmpty())
                    return;
            }
        }
    }
}