package com.tos.server.api;

import com.google.gson.JsonElement;

/**
 * This interface represents a templater that replaces json elements.
 * @param <R> the type of JsonElement to replace.
 */
public interface IJsonReplaceable<R extends JsonElement> {
    /**
     * This function replaces a JsonElement with another one of the same type.
     * @param name the name of the element.
     * @param element the element to replace.
     * @return the replacement for the element given.
     */
    R replace(String name,R element);
}
