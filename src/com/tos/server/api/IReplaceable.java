package com.tos.server.api;

/**
 * This interface represents a template replacer.
 * @param <S> the type of data the replaceable works with.
 */
public interface IReplaceable<S> {
    /**
     * This function checks if S should be replaced.
     * @param data the data to replace.
     * @return true if the data should be replaced, false otherwise.
     */
    boolean canReplace(S data);

    /**
     * This function takes data and returns a replacement.
     * @param data the data to replace.
     * @return the data to replace the input.
     */
    S replace(S data);
}
