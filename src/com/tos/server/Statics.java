package com.tos.server;

import com.tos.server.provider.FileServer;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.sql.Connection;

/**
 * This class is a singleton for all the configurations.
 */
public class Statics {
    //The implementation version
    public static final String ImplVersion = "0.03";
    //The version of the api of the server used for compatibility check
    public static final String ApiVersion = "0.02";
    //The port over which the server is communicating.
    public static int port;
    //The time(in microseconds) it took to setup.
    public static double startup;
    //The working directory.
    public static String runningPath = Paths.get("").toAbsolutePath().normalize().toString();
    //The Object tasked with serving all html files.
    public static final FileServer fileServer = new FileServer();
    //The Connection to the dataBase
    public static Connection dbConnection;
    //A flag to enable cache, enabled by default
    public static boolean cacheEnabled = true;
    //A flag to enable the console window, enabled by default.
    public static boolean consoleEnabled = true;
    //The stream some actions like DataUtil#exportData use fot output
    public static OutputStream out;
    //The stream some actions like DataUtil#exportData use fot input
    public static InputStream in;
//    public static Enc encryptor;
}
