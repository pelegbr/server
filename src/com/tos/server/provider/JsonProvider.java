package com.tos.server.provider;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.tos.server.Statics;
import com.tos.server.api.Http.RequestType;
import com.tos.server.api.IJsonReplaceable;
import com.tos.server.util.DBUtil;
import com.tos.server.util.HttpUtil;
import com.tos.server.util.ResourceUtil;
import com.tos.server.util.SerialUtil;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

/**
 * This class is a part of the templating system, it handles a request from client and serve file after replacing the templates inside them.
 * It does so in the protocol inscribed in {@link ResponseProvider}
 */
public class JsonProvider implements ResponseProvider {
    //The templater object.
    private final JsonReplacer replaceable;
    //A parser for json files.
    private JsonParser parser;
    //Caching statements for performance.
    PreparedStatement testCheck = null;
    //A modifier function.
    AtomicReference<BiFunction<String, String, String>> replacer = new AtomicReference<>((value, name) -> {
        if (value.contains("%USERID%")) {
            String s = null;
            try {
                ResultSet set = DBUtil.get(Statics.dbConnection, "SELECT Id FROM Users WHERE Name = \"" + name + "\"");
                s = set.getInt("Id") + "";
                DBUtil.closeSet(set);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            if(s != null)
                value = value.replaceAll("%USERID%", s);
        }
        value = value.replaceAll("%USERTXT%","שלום " + name + ".");
        return value;
    });

    /**
     * The default constructor.
     */
    public JsonProvider() {
        replaceable = new JsonReplacer(replacer);
        parser = new JsonParser();
    }

    //It can only POST request.
    @Override
    public boolean canHandle(RequestType type) {
        return type == RequestType.POST;
    }

    /**
     * This function parses the post data and then relays the request to {@link #serve(String, Map, HttpExchange)}.
     */
    @Override
    public boolean tryServe(String request, HttpExchange httpExchange, final byte... params) {
        String res = new String(SerialUtil.trimBytes(params));
        Map<String, String> args = HttpUtil.getParameters(res);
        return serve(args.remove("requestType"), args, httpExchange);
    }

    /**
     * This function loads a json file, userinfo and then activating the templates in it.
     */
    @Override
    public boolean serve(String request, Map<String, String> args, HttpExchange httpExchange) {
        boolean success = false;
        String Response = null;
        switch (request) {
            case "login":
            case "mainPage":
                //Load the main page/login.
                Response = DBUtil.checkUserDetails(args);
                if (Response == null) {
                    if (replaceable.busyCount > 0) {
                        Response = ResourceUtil.readJson("/Main.re.json", new JsonReplacer(replacer) {{
                            name = args.get("Username");
                        }}).toString();
                    } else {
                        replaceable.name = args.get("Username");
                        Response = ResourceUtil.readJson("/Main.re.json", replaceable).toString();
                    }
                }
                success = true;
                break;
            case "new2nd":
                //Load the test request page.
                Response = ResourceUtil.readJson("/new2nd.json").toString();
                success = true;
                break;
            case "sendChoices":
                //Send the available choices matching the data from the client.
                JsonArray array = null;
                try {
                    array = (JsonArray) parser.parse(args.get("chose"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Response = retrieveNextOption(array).toString();
                success = true;
                break;
            case "sendTable":
                //Send a table of all students matching the data from the client.
                JsonArray chose = null;
                try {
                    chose = (JsonArray) parser.parse(args.get("chose"));
                } catch (JsonIOException ex) {
                    ex.printStackTrace();
                }
                Response = getTable(chose).toString();
                success = true;
                break;
            case "newTests":
                //Handle the creation of tests.
                success = true;
                JsonArray object;
                object = (JsonArray) parser.parse(args.get("par"));
                Response = DBUtil.addTests(args.get("me"), object);
                if (Response == null) {
                    if (replaceable.busyCount > 0)
                        Response = ResourceUtil.readJson("/Main.re.json", new JsonReplacer(replacer) {{
                            name = args.get("Username");
                        }}).toString();
                    else {
                        replaceable.name = args.get("Username");
                        Response = ResourceUtil.readJson("/Main.re.json", replaceable).toString();
                    }
                }
                break;
            case "removeTests":
                //Handle the deletion of tests.
                success = true;
                JsonArray checked = (JsonArray) parser.parse(args.get("checked"));
                DBUtil.removeTests(args.get("me"), checked);
                Response = "{}";
                break;
        }
        //Send the response and cleanup.
        if (Response != null) {
            try {
                byte[] b = Response.getBytes();
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, b.length);
                httpExchange.getRequestBody().close();
                httpExchange.getResponseBody().write(b);
                httpExchange.getResponseBody().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return success;
    }


    /**
     * This function gets all students with data matching the request(class, layer, and profession).
     *
     * @param chose all the data to match.
     * @return
     */
    private JsonArray getTable(JsonArray chose) {
        JsonArray dest = new JsonArray();
        //100 Ids for each layer in school + 100(it starts at 100 and not 0) + the relative id(A1,A2,A3...).
        int classId = 100 + chose.get(1).getAsInt() + chose.get(2).getAsInt();
        try {
            //Prepare the statement if it's not prepared already.
            if (testCheck == null)
                testCheck = DBUtil.prepare(Statics.dbConnection, "SELECT StudentId FROM Tests WHERE StudentId = ? AND ProId = ?");
            ResultSet set = DBUtil.get(Statics.dbConnection, "SELECT * FROM Students WHERE Class = " + classId + " ORDER BY Students.Id");

            //For each student.
            while (set.next()) {
                int studentId = set.getInt("Id");

                testCheck.setInt(1, studentId);
                testCheck.setInt(2, chose.get(0).getAsInt());
                ResultSet testOverride = testCheck.executeQuery();

                //Check if the student doesn't have that test already.
                if (testOverride.isClosed()) {
                    dest.add(new JsonPrimitive(studentId));
                    dest.add(new JsonPrimitive(set.getString("Name")));
                } else
                    //Cleanup
                    testOverride.close();
            }
            //Cleanup
            DBUtil.closeSet(set);
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return dest;
    }

    /**
     * This function retrieves the next options to choose in the next dropdown menu.
     *
     * @param chose the object representing the user's progress in the filling of the form.
     * @return the options of the next options for the dropdown menu.
     */
    public JsonArray retrieveNextOption(JsonArray chose) {
        JsonArray dest = new JsonArray();
        switch (chose.size()) {
            case 0:
                //If the form is empty get the professions available.
                ResultSet professions = DBUtil.get(Statics.dbConnection, "SELECT * FROM Professions");
                try {
                    while (professions.next()) {
                        JsonObject object = new JsonObject();
                        object.add("text", new JsonPrimitive(professions.getString("Name")));
                        object.add("value", new JsonPrimitive(professions.getInt("Id")));
                        dest.add(object);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                DBUtil.closeSet(professions);
                break;
            case 1:
                //If the profession is chosen get the Layers.
                ResultSet layers = DBUtil.get(Statics.dbConnection, "SELECT * FROM Layers");
                try {
                    while (layers.next()) {
                        JsonObject object = new JsonObject();
                        object.add("text", new JsonPrimitive(layers.getString("Name")));
                        object.add("value", new JsonPrimitive(layers.getInt("Id")));
                        dest.add(object);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                DBUtil.closeSet(layers);
                break;
            case 2:
                //If the layer is chosen get the classes afterwards in goes to getTable.
                ResultSet students = DBUtil.get(Statics.dbConnection, "SELECT * FROM Layers where Id = " + chose.get(1));
                try {
                    for (int i = 1; i <= students.getInt("Count"); i++) {
                        JsonObject object = new JsonObject();
                        object.add("text", new JsonPrimitive(students.getString("name") + i));
                        object.add("value", new JsonPrimitive(i));
                        dest.add(object);
                    }

                } catch (SQLException sql) {
                    sql.printStackTrace();
                }
                DBUtil.closeSet(students);
                break;
        }
        return dest;
    }

    /**
     * This class is the implementer {@link IJsonReplaceable<JsonElement>}
     * It is responsible for most of templating in the server side.
     */
    public static class JsonReplacer implements IJsonReplaceable<JsonElement> {
        private AtomicReference<BiFunction<String, String, String>> replacer;
        protected String name;
        private int busyCount;

        public JsonReplacer(AtomicReference<BiFunction<String, String, String>> replacer) {
            if (replacer == null) this.replacer = new AtomicReference<>((v, n) -> v);
            else this.replacer = replacer;
        }

        /**
         * This function is where all the data passes before being sent.
         * This is where the templating happens.
         */
        @Override
        public JsonElement replace(String name, JsonElement value) {
            busyCount++;
            //Foreach, a database statement needs to go inside this element(Object).
            if (name.charAt(0) == '@') {
                if (!(value instanceof JsonArray)) return value;
                JsonArray dest = new JsonArray();
                JsonArray array = (JsonArray) value;
                StringBuilder builder = new StringBuilder();
                build(builder, array);

//                ResultSet set = DBUtil.get(Statics.dbConnection, "SELECT " + builder + " ORDER BY ProId,StudentId ASC");
                ResultSet set = DBUtil.get(Statics.dbConnection, "SELECT " + builder);
                if (set == null) return dest;
                try {
                    boolean b = !set.isClosed();
                    set.next();
                    while (b) {
                        for (int i = 0; i < array.size(); i++) {
                            String str = array.get(i).getAsString();
                            if (str.startsWith("#include") || str.startsWith("!")) continue;
                            //Database Command
                            if (str.charAt(0) == '~') {
//                                String str = array.get(i).getAsString();
                                dest.add(Logic.next(str.substring(1)));
                            } else if (str.charAt(0) == '$') {
                                //complex statement.
                                String output = null;
                                String[] el = array.get(i).getAsString().substring(1).split(":");
                                Object o = set.getObject(el[2]);
                                ResultSet set1;

                                if (str.charAt(1) == '!') {
                                    set1 = DBUtil.get(Statics.dbConnection, "SELECT " + el[3] + " FROM " + el[0].substring(1) + " WHERE " + el[1] + "=" + o);
                                    int c = set1.getInt(el[3]);
                                    switch (c / 100) {
                                        case 1:
                                            output = "ט" + c % 100;
                                            break;
                                        case 2:
                                            output = "י" + c % 100;
                                            break;
                                        case 3:
                                            output = "יא" + c % 100;
                                            break;
                                        case 4:
                                            output = "יב" + c % 100;
                                            break;
                                    }
                                } else
                                    set1 = DBUtil.get(Statics.dbConnection, "SELECT " + el[3] + " FROM " + el[0] + " WHERE " + el[1] + "=" + o);
                                if (output == null)
                                    output = set1.getString(el[3]);
                                dest.add(output);
                                DBUtil.closeSet(set1);
                            } else {

                                Object object = set.getObject(str);
                                if (object == null)
                                    dest.add("");
                                else if (object instanceof Integer)
                                    dest.add((Integer) object);
                                else if (object instanceof String)
                                    dest.add((String) object);

                            }
                        }

                        b = set.next();
                    }
                    Logic.reset();
                    DBUtil.closeSet(set);
                } catch (SQLException sql) {
                    sql.printStackTrace();
                }
                busyCount--;
                return dest;
            }

            //Replacing text inside an element.
            if (value instanceof JsonPrimitive) {
                String valueString = value.getAsString();
                return valueString.charAt(0) == '%' && valueString.charAt(valueString.length() - 1) == '%'
                        ? new JsonPrimitive(replacer.get().apply(valueString, this.name)) : value;
            }

            //Recursive operation over all fields in the array
            if (value instanceof JsonObject) {
                for (Map.Entry<String, JsonElement> entry : ((JsonObject) value).entrySet()) {
                    entry.setValue(replace(entry.getKey(), entry.getValue()));
                }
            }

            return value;
        }

        /**
         * Builds a database query based on input.
         * @param builder the build to append the query to.
         * @param element the JsonArray containing the query pieces.
         */
        private void build(StringBuilder builder, JsonArray element) {
            Iterator<JsonElement> iterator = element.iterator();
            String nString = null;
            String curString = iterator.next().getAsString();
            boolean b = true;

            while (b) {
                curString = nString == null ? curString : nString;
                if (curString.charAt(0) == '$' || curString.charAt(0) == '~') {
                    b = iterator.hasNext();
                    if (b)
                        nString = iterator.next().getAsString();
                    continue;
                }
                if (curString.charAt(0) == '!')
                    builder.append(' ').append(replacer.get().apply(curString.substring(1), name));
                else {
                    if (curString.startsWith("#include"))
                        builder.append(curString.substring(8));
                    else
                        builder.append(curString);
                }

                if (b = iterator.hasNext()) {
                    nString = iterator.next().getAsString();

                    if (curString.charAt(0) != '!' && nString.charAt(0) != '!' && nString.charAt(0) != '$')
                        builder.append(", ");
                }
            }
        }
    }

    /**
     * This class is a utility for alternating patterns.
     */
    public static final class Logic {
        private static int i = 0;

        /**
         * This dictates what label should be chosen next in the alternating pattern.
         * @param pattern the alternating pattern.
         * @return the next label to choose.
         */
        public static String next(String pattern) {
            int options = (int) pattern.chars().filter(c -> c == '|').count() + 1;
            return pattern.split("[|]")[i = (i + 1) % options];
        }

        /**
         * This function reset the alternating pattern.
         */
        public static void reset() {
            i = 0;
        }
    }
}
