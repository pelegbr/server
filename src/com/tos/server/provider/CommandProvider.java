package com.tos.server.provider;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.Statics;
import com.tos.server.api.Http.RequestType;
import com.tos.server.console.commands.Common.CommonCommand;
import com.tos.server.console.commands.Common.CommonCommands;
import com.tos.server.console.commands.ICommand;
import com.tos.server.console.commands.ICommandExec;
import com.tos.server.console.manager.ConsoleManager;
import com.tos.server.util.HttpUtil;
import com.tos.server.util.SerialUtil;

import javax.net.ssl.HttpsURLConnection;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class CommandProvider implements ResponseProvider {

    //It can always handle a request.
    @Override
    public boolean canHandle(RequestType type) {
        return true;
    }

    /**
     * This function uses post data, so it is parse in here before calling {@link #serve(String, Map, HttpExchange)}.
     */
    @Override
    public boolean tryServe(String request, HttpExchange httpExchange, final byte... args) {
        String res = new String(SerialUtil.trimBytes(args)).trim();
        //If it just contains 'command' it can't be handled.
        if (res.length() < 7) return false;
        String quarry = res.substring(7);
        Map<String, String> parameters = HttpUtil.getParameters(res);
        if (parameters.remove("requestType").equals("command")) {
            return serve(parameters.remove("code"), parameters, httpExchange);
        }
        return false;
    }

    /**
     * This function try to execute a command given by client.
     */
    @Override
    public boolean serve(String request, Map<String, String> args, HttpExchange httpExchange) {
        //Try find the requested command.
        Object[] cmdArgs = args.get("args").split(",");
        //Find a command with requested name
        Stream<ICommandExec<?>> stream = CommonCommands.stream(request);
        try {
            //Attempt to execute the command.
            if (stream == null) {
                //Error if the server could not find the command.
                httpExchange.sendResponseHeaders(400, 0);
                httpExchange.getResponseBody().close();
                ConsoleManager.instance.println("Attempted to invoke an unknown command :" + request + " with args: " + args.entrySet());
            } else {
                //Find a command that matches the given parameters.
                ICommandExec<?> c = stream.filter(exec -> exec.accepts(cmdArgs)).findFirst().orElse(CommonCommand.EMPTY.getExec());
                switch (request) {
                    case "export":
                        if (cmdArgs.length != 2) break;
                        httpExchange.getResponseHeaders().put("Content-Disposition", Collections.singletonList("attachment; filename=" + request + cmdArgs[0] + ";"));
                        Statics.out = httpExchange.getResponseBody();
                        //Cleanup
                        httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_OK, 0);
                        c.exec(ConsoleManager.instance.getMain(), cmdArgs);
                        httpExchange.getResponseBody().close();
                        return true;
                    case "import":
                        Statics.in = new ByteArrayInputStream(args.get("in").getBytes());
                        if (cmdArgs.length != 2) break;
                        c.exec(ConsoleManager.instance.getMain(), cmdArgs);
                        //Cleanup
                        httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_OK, 0);
                        httpExchange.getResponseBody().close();
                        return true;
                }
                //Error executing the command.
                String response = ("An error occurred whilst executing command: " + request + " with args: " + Arrays.toString(cmdArgs));
                //Cleanup
                httpExchange.sendResponseHeaders(HttpsURLConnection.HTTP_BAD_REQUEST, response.getBytes().length);
                httpExchange.getResponseBody().write(response.getBytes());
                httpExchange.getResponseBody().close();
            }
        } catch (IOException ex) {
        }
        return true;
    }
}
