package com.tos.server.provider;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.Server;
import com.tos.server.api.Http.RequestType;
import com.tos.server.api.annotation.ImplMember;
import com.tos.server.api.Http.VirtualRequest;
import com.tos.server.Statics;
import sun.security.util.Cache;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

@ImplMember(Versions = Statics.ImplVersion)
/**
 * This class provides file via the {@link ResponseProvider} protocol.
 * It can also serve statically if you so choose.
 */
public class FileServer implements ResponseProvider {
    //Hot cache.
    public static final Cache<String, byte[]> fileCache = Cache.newSoftMemoryCache(36, 1800);

    @Override
    //It can handle only GET Requests.
    public boolean canHandle(RequestType type) {
        return type == RequestType.GET;
    }

    /**
     * Attemps to get a resource if it fails return false, else pass the request to {@link #serve(String, Map, HttpExchange)}
     */
    public boolean tryServe(String request, HttpExchange httpExchange, final byte... params) {
        if (request.replace("/", "").isEmpty()) return false;
        URL resource = Server.class.getResource("resources" + request);
        return resource != null && serve("resources" + request, null, httpExchange);
    }

    /**
     * Passes the request to {@link #ServeFile(String, int, HttpExchange)}
     */
    public boolean serve(String request, Map<String, String> args, HttpExchange httpExchange) {
        return ServeFile(request, HttpURLConnection.HTTP_OK, httpExchange);
    }

    /**
     * This function is a static form of {@link #tryServe(String, HttpExchange, byte...)}.
     * @param request the request URI.
     * @param response the Status to respond with if succeeded.
     * @param httpExchange the http exchange for this request.
     * @return whether it succeeded or not.
     */
    public static boolean tryServeFile(String request, int response, HttpExchange httpExchange) {
        if (request.replace("/", "").isEmpty()) return false;
        URL resource = Server.class.getResource("resources" + request);
        return resource != null && ServeFile("resources" + request, response, httpExchange);
    }

    /**
     * This function reads a file from the cache or disk and serves it to a client.
     * @param File the file to read.
     * @param response the response to send if succeeded.
     * @param httpExchange the http exchange for this request.
     * @return whether it succeeded or not.
     */
    public static boolean ServeFile(String File, int response, HttpExchange httpExchange) {
        OutputStream stream = httpExchange.getResponseBody();
        try {
            byte[] result = Statics.cacheEnabled ? fileCache.get(File) : null;
            //Chucked reading the file if not in cache.
            if (result == null) {
                int i;
                byte[] b = new byte[8192];

                InputStream inputStream = Server.class.getResourceAsStream(File);
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();

                while ((i = inputStream.read(b)) != -1) {
                    byteStream.write(b, 0, i);
                }
                result = byteStream.toByteArray();
                if (Statics.cacheEnabled)
                    fileCache.put(File, result);

                byteStream.close();
                inputStream.close();
            }

            //Determine the mine of the file.
            String mime = File.endsWith(".css") ? "text/css" : "text/html";
            httpExchange.getResponseHeaders().put("Content-Type", Collections.singletonList(mime));

            httpExchange.sendResponseHeaders(response, result.length);
            //Send.
            stream.write(result);
            //Cleanup
            stream.close();
            return true;
        } catch (Exception e) {
            //error handling
            try {
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
            return false;
        }
    }
}
