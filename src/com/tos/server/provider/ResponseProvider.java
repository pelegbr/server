package com.tos.server.provider;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.Statics;
import com.tos.server.api.Http.RequestType;
import com.tos.server.api.annotation.ApiMember;

import java.util.Map;

/**
 * class ResponseProvider represents a possible response to the client's request,
 * which is handled by {@link com.tos.server.api.Http.VirtualHandler} and then passed
 * to the implementation of this class.
 * it consists of three methods:
 */
@ApiMember(Versions = Statics.ApiVersion)
public interface ResponseProvider {
    /**
     * This function determines whether a request can be handled by this handler.
     * @param type the type of request being made.
     * @return true if the handler can handle this type or false if it can't.
     */
    boolean canHandle(RequestType type);

    /**
     * This function tries to serve a request, it should not cause an error should the request be invalid.
     * @param request the type of request see {@link com.tos.server.api.Http.RequestType}
     * @param httpExchange the exchange of http of the request.
     * @param args the arguments given in post, if at all.
     * @return whether the request succeeded.
     */
    boolean tryServe(String request, HttpExchange httpExchange, final byte... args);

    /**
     * This function is a (mostly) internal handling of a request, could cause an error.
     * @param request the request string.
     * @param args the argument map parsed by {@link #tryServe(String, HttpExchange, byte...)}, decided internally.
     * @param httpExchange the exchange of http of the request.
     * @return whether the request succeeded.
     */
    boolean serve(String request, Map<String, String> args, HttpExchange httpExchange);

// part of a rewrite,
//    boolean handles(VirtualRequest request);
//
//    /**
//     * try to handle the request and serve the right resource back.
//     *
//     * @param request the data of the request.
//     */
//    void serve(VirtualRequest request) throws IOException;
}
