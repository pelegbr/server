package com.tos.server.provider;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.api.Html.Page;
import com.tos.server.api.Http.RequestType;
import com.tos.server.api.annotation.ImplMember;
import com.tos.server.api.Http.VirtualRequest;
import com.tos.server.Statics;
import com.tos.server.util.HttpUtil;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Class PageProvider Represents a {@link ResponseProvider},
 * PageProvider Manages Standalone Pages and GET requests from the client.
 * see {@link ResponseProvider} for more coverage.
 */
@ImplMember(Versions = Statics.ApiVersion)
public class PageProvider implements ResponseProvider {
    public final List<Page> pages;

    public List<Page> getPages() {
        return pages;
    }

    //Default pages.
    public PageProvider() {
        pages = new ArrayList<>();
        pages.add(new Page("/", "/Response.htm"));
        pages.add(new Page("/about", "/about.htm"));
        pages.add(new Page("/2ndTest", "/new2nd.htm"));
        pages.add(new Page("/io", "/io.htm"));
    }

    //Add pages.
    public PageProvider(Page... pages) {
        this.pages = Arrays.asList(pages);
    }

    //Can handle only GET requests
    @Override
    public boolean canHandle(RequestType type) {
        return type == RequestType.GET;
    }

    /**
     * Parses the query string and relays the request to {@link #serve(String, Map, HttpExchange)}
     */
    @Override
    public boolean tryServe(String request, HttpExchange httpExchange, final byte... params) {
        Map<String, String> args = HttpUtil.getParameters(httpExchange.getRequestURI().getQuery());
        return serve(request, args, httpExchange);
    }

    @Override
    /**
     * Gets what file is needed to be served and passes the request to {@link FileServer#tryServeFile(String, int, HttpExchange, String...)}
     */
    public boolean serve(String request, Map<String, String> args, HttpExchange httpExchange) {
        if (httpExchange.getRequestMethod().equals("GET"))
            for (Page p : pages) {
                if (p.getPath().equals(request))
                    return FileServer.tryServeFile(p.getFilePath(), HttpsURLConnection.HTTP_OK, httpExchange);
            }
        return false;
    }
}
