package com.tos.server.provider;

import com.sun.net.httpserver.HttpExchange;
import com.tos.server.api.Http.RequestType;
import com.tos.server.api.annotation.ImplMember;
import com.tos.server.api.Http.VirtualRequest;
import com.tos.server.util.HttpUtil;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

@ImplMember(Versions = "@all")

/**
 * This class is a {@link ResponseProvider} that is the fallback in case all other providers fail/can't serve a request.
 * (For example an non-existent resource request).
 */
public class fallbackProvider implements ResponseProvider {
    @Override
    //It can always handle a request.
    public boolean canHandle(RequestType type) {
        return true;
    }

    /**
     * Delegate the request {@link #serve(String, Map, HttpExchange)}.
     */
    @Override
    public boolean tryServe(String request, HttpExchange httpExchange, final byte... args) {
        return serve(request, HttpUtil.getParameters(request), httpExchange);
    }

    /**
     * Delegate the request {@link FileServer#tryServeFile(String, int, HttpExchange, String...)}.
     */
    @Override
    public boolean serve(String request, Map<String, String> args, HttpExchange httpExchange) {
        return FileServer.tryServeFile("/fallback.htm", HttpURLConnection.HTTP_NOT_FOUND, httpExchange);
    }
}
